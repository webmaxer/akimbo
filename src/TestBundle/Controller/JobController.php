<?php

namespace TestBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use TestBundle\Repository\JobRepository;

class JobController extends Controller
{

    /**
     * @Route("/jobs", name="jobs")
     */
    public function jobsAction(Request $request)
    {
        $em      = $this->get('doctrine.orm.entity_manager');
        $session = $request->getSession();

        return $this->render('TestBundle:Default:jobs.html.twig', [
        	'industries' => $em->getRepository('TestBundle:Industry')->findBy([], ['industryName' => 'ASC']),
        	'job_types'  => $em->getRepository('TestBundle:JobType')->findBy([], ['jobTypeTitle' => 'ASC']),
        	'jobs'       => $em->getRepository('TestBundle:Job')->findAll()
        ]);
    }

    /**
     * @Route("/jobs/filters/", name="jobs_filters")
     */
    public function filtersAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $filters = $request->get('filters');

        //to check filter values
        $filters['schedule']   = isset($filters['schedule'])   ? $filters['schedule']   : [];
        $filters['industries'] = isset($filters['industries']) ? $filters['industries'] : [];

        //find jobs by fiilters
        $jobs = $em->getRepository('TestBundle:Job')->filters([
        	JobRepository::FILTER_SORT     => $filters['sort'],
        	JobRepository::FILTER_TITLE    => $filters['title'],
        	JobRepository::FILTER_POSTED   => $filters['posted'],
        	JobRepository::FILTER_SALARY   => $filters['salary'],
        	JobRepository::FILTER_SCHEDULE => $filters['schedule'],
        	JobRepository::FILTER_LOCATION => $filters['location'],
        	JobRepository::FILTER_INDUSTRY => $filters['industries'],
    	]);

        return new JsonResponse([
            'jobs' => $this->get('app.serializer')->serializeJobs($jobs)
        ]);
    }
}