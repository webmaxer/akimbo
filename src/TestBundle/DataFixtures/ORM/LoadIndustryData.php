<?php

namespace TestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TestBundle\Entity\Industry;

class LoadIndustryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $industries = ['accounting', 'administrative-support', 'arts-and-entertainment', 'construction',
            'educational-services', 'general', 'healthcare', 'information-technology', 'maintenance',
            'manufacturing', 'media-and-design', 'real-estate', 'restaurants-bars-and-food-service',
            'retail', 'transportation-delivery', 'utilities'
        ];

        foreach ($industries AS $nameIndustry) {
            $industry = new Industry();
            $industry->setIndustryName($nameIndustry);
            $industry->setIndustryImage('/images/industry/'.$nameIndustry.'.svg');

            $manager->persist($industry);   
        }

        $manager->flush();

        //$this->addReference('admin-user', $userAdmin);
    }

    public function getOrder()
    {
        return 0;
    }
}