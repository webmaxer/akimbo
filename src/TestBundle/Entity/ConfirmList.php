<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfirmList
 *
 * @ORM\Table(name="confirm_list")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\ConfirmListRepository")
 */
class ConfirmList
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="confirm")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="activationKey", type="string", length=255)
     */
    private $activationKey;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activationKey
     *
     * @param string $activationKey
     * @return ConfirmList
     */
    public function setActivationKey($activationKey)
    {
        $this->activationKey = $activationKey;

        return $this;
    }

    /**
     * Get activationKey
     *
     * @return string 
     */
    public function getActivationKey()
    {
        return $this->activationKey;
    }

    /**
     * Set userId
     *
     * @param \TestBundle\Entity\User $userId
     * @return ConfirmList
     */
    public function setUserId(\TestBundle\Entity\User $userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return \TestBundle\Entity\User 
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
