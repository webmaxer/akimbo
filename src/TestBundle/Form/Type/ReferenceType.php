<?php 


namespace TestBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fullName');
        $builder->add('jobTitle');
        $builder->add('organization');
        $builder->add('relationship');
        $builder->add('phone', 'text', array(
            'error_bubbling' => true
        ));
        $builder->add('email', 'text', array(
            'error_bubbling' => true
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Reference',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'reference';
    }
}