
	
	// <li class="profile-item">
	//   <div>
	//     <p><b>Job title:</b> {jobTitle}</p>
	//     <p><b>Company name:</b> {companyName}</p>
	//     <p><b>Date started:</b> {dateFinished}</p>
	//     <p><b>Date finished:</b> 
	//       {% if item.isCurrent %}
	//         This is my current job
	//       {% else %}
	//         {{item.dateFinished |date("d/m/Y")}}
	//       {% endif %}  
	//     </p>
	//     <p><b>Location:</b> {{item.location}}</p>
	//   </div>
	// </li>


var addFieldset = function($collection) {
		// Get the data-prototype explained earlier
		var prototype = $collection.data('prototype');

		// get the new index
		var index = parseInt($collection.attr('data-index'));

		// Replace '__name__' in the prototype's HTML to
		// instead be a number based on how many items we have
		var $newForm = prototype.replace(/__name__/g, index);

		// increase the index with one for the next item
		$collection.attr('data-index', parseInt(index + 1));

		// Display the form in the page in an li, before the "Add a tag" link li
		$container = $collection.find('.fields-container');
		$container.append($newForm);

		datePickerHandler();
		geoInputHandler();
		bindRemoveFieldSet();
		materialSelectHandler();
		wysiwygHandler();
		disableInput();
	}
	// End addFieldset

var bindRemoveFieldSet = function() {
	$('.remove_fieldset').click(function(event) {
		event.preventDefault();
		var whatRemove = $(this).attr('href');
		$(whatRemove).remove();
	});
}

var collectionHolder = function() {


		$.each($('.js-fieldset'), function(index, val) {

			// count the current form inputs we have (e.g. 2), use that as the new
			// index when inserting a new item (e.g. 2)
			$(val).attr('data-index', $(val).find(':input').length);


		});
		$('.js-add-fieldset').on('click', function(e) {
			e.preventDefault();
			$collection = $($(this).attr('href'));
			// add a new tag form (see addFieldset() code block)
			addFieldset($collection);
		});


	}
	// End collectionHolder

var editFieldsHandler = function() {
	$('.js-edit-fields').click(function(event) {
		event.preventDefault();
		var $parent = $(this).closest('.js-fields');

		$parent.addClass('edit-mode').removeClass('view-mode');

		$parent.on('click', '.js-edit-cancel', function(event) {
			event.preventDefault();
			$parent.removeClass('edit-mode').addClass('view-mode');

		});
	});
}




var closeErrorHandler = function() {
	$('.js-close-error').click(function(event) {
		$parent = $(this).closest('.build-section');
		$parent.fadeOut(250);
		setTimeout(function() {$parent.remove()}, 250);
	});
}

var	disableInput = function() {
	$('.js-disable-input').change(function(event) {
		input = $(this).data('disable');
		$input = $('#'+input);
		if($(this).prop('checked')) {
			$input.prop('disabled', true);
		} 
		else {
			$input.prop('disabled', false);
		}
	});
}

App.profileTemplating = function() {
	collectionHolder();
	bindRemoveFieldSet();
	editFieldsHandler();
	closeErrorHandler();
	disableInput();
}