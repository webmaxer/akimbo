<?php 


namespace TestBundle\Form\Type;

use TestBundle\Entity\Industry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminUpdateType extends AbstractType
{

    public function __construct($em) {
        $this->em = $em;
   }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {   


        $builder->add('firstName', 'text', array(
            'error_bubbling' => true
        ));
        $builder->add('lastName', 'text', array(
            'error_bubbling' => true
        ));

        $builder->add('email', 'email', array(
            'error_bubbling' => true
        ));

        $builder->add('industry', 'entity', array(
            'class' => 'TestBundle:Industry',
            'property' => 'industryName',
            'expanded' => false,
            'multiple' => false,
            'required' => false,
            'error_bubbling' => true,
        ));

        $builder->add('location', 'text', array(
            'error_bubbling' => true,
            'required'=>false,
            'attr'=> array(
                'placeholder' => 'City, state'
            ),
        ));

        $builder->add('phone', 'text', array(
            'error_bubbling' => true,
            'required'=>false,
            'attr' => array(
                'placeholder' => 'Phone number'
            )
        ));
        $builder->add('status', 'choice', array(
            'error_bubbling' => true,
            'choices'  => array(
                'Verified' => true,
                'In progress' => false,
            ),
            'choices_as_values' => true,
        ));        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\User',
            'validation_groups' => array('admin_update'),
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'AdminUpdate';
    }
}