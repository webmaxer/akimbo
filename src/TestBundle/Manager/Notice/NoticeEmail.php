<?php

namespace TestBundle\Manager\Notice;

use TestBundle\Manager\Notice\NoticeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NoticeEmail implements NoticeInterface
{
	//constants for array data of the message
	const MESSAGE_SUBJECT = 'subject';
	const MESSAGE_TEXT    = 'text';
	const MESSAGE_TO      = 'to';
	const MESSAGE_FROM    = 'from';
	const MESSAGE_IMAGES  = 'images';
	
	/**
	 * Sfiftmailer
	 *
	 * @var \Swift_Mailer
	*/
	private $mailer;

	/**
	 * Sfiftmailer message
	 *
	 * @var \Swift_Message
	*/
	private $message;

	/**
	 * Service container
	 *
	 * @var \Symfony\Component\DependencyInjection\ContainerInterface
	*/
	private $container;

	/**
	 * The sender
	 *
	 * @var array
	*/
	private $from = array('mail@akimboworks.com' => 'Team Akimboworks.com');

	/**
	 * Constuctor
	 *
	 * @param \Swift_Mailer $mailer
	 * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
	*/
	public function __construct(\Swift_Mailer $mailer, ContainerInterface $container)
	{
		$this->mailer    = $mailer;
		$this->container = $container;
	}

	public function createMessageByTemplate($templateName, array $templateData)
	{
		$this->message = \Swift_Message::newInstance();

		$images = [];

		if (isset($templateData[self::MESSAGE_IMAGES])) {
			foreach ($templateData[self::MESSAGE_IMAGES] AS $key => $image) {
				$images[$key] = $this->message->embed(\Swift_Image::fromPath($image));
			}
		}

		$messageContent = $this->container->get('templating')->render($templateName.'.html.twig', array_merge($templateData, ['images' => $images]));

		$this->message->setBody($messageContent, 'text/html');

		return $this;
	}

	public function createMessageByText($messageContent)
	{
		$this->message = \Swift_Message::newInstance();
		$this->message->setBody($messageContent, 'text/plain');

		return $this;
	}

	/**
	 * Send message
	 *
	 * @param array $dataMessage
	*/
	public function send(array $dataMessage)
	{
	    $this->message->setSubject($dataMessage[self::MESSAGE_SUBJECT])
				      ->setFrom($this->from)
				      ->setTo($dataMessage[self::MESSAGE_TO]);

		$this->mailer->send($this->message);
	}
}