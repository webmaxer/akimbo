Backbone.history.start();
Backbone.emulateHTTP = true;
 
var jobsCollection = new JobsCollection();
//reviewsCollection.fetch({ data: { 'type':'new','page':1 } });
jobsCollection.fetch();

//var paginationView  = new PaginationView();
var jobsListView = new JobsListView({ collection: jobsCollection });
jobsListView.render();

$('#add_job').click(function(){

	var industries = [];
	var types      = [];
	
	$('[name="industries"]:checked').each(function(){
		industries.push($(this).val());
	});

	$('[name="types"]:checked').each(function(){
		industries.push($(this).val());
	});

	var job = new JobModel({
		jobTitle: $('#jobTitle').val(),
		salary: $('#salary').val(),
		description: $('#description').val(),
		location: $('#location').val(),
		company: $('#company').val(),
		industries: industries,
		jobTypes: types,
	});

	job.save(jobsCollection);
});

$('#edit_job').click(function(){

	var job = jobsCollection.get($('#edit_form_job').attr('data-id'));

	var industries = [];
	var types      = [];
	
	$('[name="edit_industries"]:checked').each(function(){
		industries.push($(this).val());
	});

	$('[name="edit_types"]:checked').each(function(){
		types.push($(this).val());
	});

	job.set('jobTitle', $('#edit_jobTitle').val());
	job.set('salary', $('#edit_salary').val());
	job.set('description', $('#edit_description').val());
	job.set('location', $('#edit_location').val());
	job.set('company', $('#edit_company').val());
	job.set('industries',industries);
	job.set('jobTypes', types);
	
	job.update();
});