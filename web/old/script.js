// App scope
var App = {};

var geoInputHandler = function() {
  var $autocomplete = $('.js-location');
  $autocomplete.parent().addClass('autocomplete');
  $.each($autocomplete, function(index, val) {
    $(val).materialize_autocomplete({
      limit: 15,
      multiple: {
        enable: false,
      },
      appender: {
        tagName: 'span'
      },
      dropdown: {
        className: 'dropdown-content ac-dropdown'
      },
      getData: function(value, callback) {
        jQuery.getJSON(
          "http://gd.geobytes.com/AutoCompleteCity?callback=?&filter=US&template=<geobytes%20city>,%20<geobytes%20code>&q=" + value,
          function(data) {
            if (data[0] != '%s') {
              var numeric_array = new Array();
              for (var i = 0; i < data.length; i++) {
                numeric_array.push({
                  'id': i,
                  'text': data[i]
                });
              }
              callback(value, numeric_array);
            }
            if (data[0] == '') {
              $('.ac-dropdown').hide()
            }
          }
        );

      }
    });
  });
}
// End geoInputHandler

var handleSlider = function() {
	var slider = $('.steps-container').bxSlider({
		'pager': false,
		'controls': false,
		'infiniteLoop': false
	});
	$('.js-slider-next, .mc-card, #GoToStep3').click(function(event) {
    event.preventDefault();
		var curSlide = slider.getCurrentSlide();

    if($(this).attr('id')=='GoToStep3') {
      curSlide = 1;
      $('.mdl-stepper-step').eq(curSlide-1).addClass('step-done');
      $('.mdl-stepper-step').eq(curSlide).addClass('step-done active-step');
      $('.mdl-stepper-step').eq(curSlide+1).addClass('active-step');
      slider.goToSlide(2);
    }
		else if (curSlide==1) {
			if ( $("#resumeform_firstName, #resumeform_lastName").valid() ) { 
				$('.mdl-stepper-step').eq(curSlide).addClass('step-done');
				$('.mdl-stepper-step').eq(curSlide+1).addClass('active-step');
				slider.goToNextSlide();
			} 
		}
		else if (curSlide==2) {
			if ( $("#resumeform_email, #resumeform_password").valid() ) { 
			 	$('.mdl-stepper-step').eq(curSlide).addClass('step-done');
			 	$('.mdl-stepper-step').eq(curSlide+1).addClass('active-step');
			 	slider.goToNextSlide();
			} 
		}
		else {
			$('.mdl-stepper-step').eq(curSlide).addClass('step-done');
			$('.mdl-stepper-step').eq(curSlide+1).addClass('active-step');
			slider.goToNextSlide();
		}

	});


	$('.js-slider-prev').click(function(event) {
    event.preventDefault();
		var curSlide = slider.getCurrentSlide();
		$('.mdl-stepper-step').eq(curSlide).removeClass('step-done active-step');
		slider.goToPrevSlide();
	});
}

var changeTheme = function() {
  // RESUME PREVIEW PAGE SLIDER
  $templates = $('.js-template-slider');
  if ($templates.length>0) {
    var $slider = $templates.bxSlider({
      'pager': false,
      'controls': false,
      onSlideBefore: function($slideElement, oldIndex, newIndex) {
        $('[data-template]').attr('data-template', newIndex+1);
      }
    });
  }

  $('.js-show-template-thumbs').click(function(event) {
    event.preventDefault();
    $('body').toggleClass('is-template-thumbs-shown');
  });

  $('[data-slide-index]').click(function(event) {
    $('[data-slide-index]').removeClass('active');
    $(this).addClass('active');
    $slider.goToSlide($(this).data('slide-index')-1);

    $.ajax({
        url: $(this).data('path'),     
        success: function(data, textStatus, xhr) {
            console.log(xhr.status);
        },
        complete: function(xhr, textStatus) {
            console.log(xhr.status);
        },
        error: function(xhr, textStatus) {
            console.log(xhr);
        } 
    });
  });
}

var stickFooter = function() {
  $(window).resize(function(event) {
    var footerHeight = $('.page-footer').outerHeight();
    $('body').css({'paddingBottom': footerHeight+'px'});
  }).resize();
}

var anchorsProfileHandle = function() {
	var $obj = $('.table-of-contents');
	if ($obj.length>0) {
		var top = $obj.offset().top - parseFloat($obj.css('marginTop').replace(/auto/, 0));
		$(window).scroll(function(event) {
			var y = $(this).scrollTop();
			if (y >= top) {
				$obj.addClass('fixed');
			} else {
				$obj.removeClass('fixed');
			}
		});
	}
}



var autoRemove = function($removed) {
  if ($removed.length>0) {
    $removed.delay(1000).fadeOut(1000);
    setTimeout(function() {$removed.remove()}, 2000);
  }
}
// End autoRemove




var materialSelectHandler = function() {
  $('select').material_select();
  var last_valid_selection = null;
  $('select').change(function(event) {
  if ($(this).val().length > 5) {
    console.log('You can only choose 5!');
    $(this).val(last_valid_selection);
    console.log(event);
  } else {
    last_valid_selection = $(this).val();
  }
});
}
// End materialSelectHandler


var datePickerHandler = function() {
  $('.datepicker').pickadate({
    changeMonth: true,
    selectYears: 120,
    selectDays: false,
    max: true,
    format: 'dd-mm-yyyy',
    yearRange: "-0:+1"
  });
}
// End datePickerHandler

var wysiwygHandler = function() {
  $('.js-wysiwyg').trumbowyg({
    btns: ['viewHTML',
    '|', 'formatting',
    '|', 'btnGrp-design',
    '|', 'link',
    '|', 'btnGrp-justify',
    '|', 'btnGrp-lists',
    '|', 'horizontalRule'],
    autogrow:true
  });
}
// End wysiwygHandler


var handleInputMask = function() {
  $('.js-phone-mask').mask("+1 (999) 999-9999");
}

App.initPlugins = function() {
  geoInputHandler();
  materialSelectHandler();
  datePickerHandler();
  wysiwygHandler();
  handleSlider();
  handleInputMask();
  $('.scrollspy').scrollSpy();
}


App.init = function() {
  stickFooter();
  anchorsProfileHandle();
  changeTheme();
  autoRemove($('#isProfileUpdated'));
}
// End App.init


jQuery(document).ready(function($) {
  App.initPlugins();
  App.init();
  App.profileTemplating();
});






