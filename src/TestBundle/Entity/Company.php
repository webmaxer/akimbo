<?php

namespace TestBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank( message = "Job title should not be blank" )
     * @ORM\Column(name="job_title", type="string", length=255)
     */
    private $jobTitle;

    /**
     * @var string
     * @Assert\NotBlank( message = "Company name should not be blank" )
     * @ORM\Column(name="company_name", type="string", length=255)
     */
    private $companyName;


    /**
     * @var string
     * @Assert\NotBlank( message = "Location should not be blank" )
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;

    /**
     * @var \Date
     * @Assert\LessThanOrEqual("now")
     * @ORM\Column(name="date_started", type="date")
     */
    private $dateStarted;

    /**
     * @var \Date
     * @Assert\LessThanOrEqual("now")
     * @ORM\Column(name="date_finished", type="date", nullable=true)
     */
    private $dateFinished;

    /**
     * @var string
     * @Assert\NotBlank( message = "Job description should not be blank" )
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="companies")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * @ORM\Column(type="boolean", name="isCurrent", nullable=false)
     */
    protected $isCurrent = false;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Company
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

   

    /**
     * Set user
     *
     * @param \TestBundle\Entity\User $user
     * @return Company
     */
    public function setUser(\TestBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TestBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     * @return Company
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string 
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Company
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set dateStarted
     *
     * @param \DateTime $dateStarted
     * @return Company
     */
    public function setDateStarted($dateStarted)
    {
        $this->dateStarted = $dateStarted;

        return $this;
    }

    /**
     * Get dateStarted
     *
     * @return \DateTime 
     */
    public function getDateStarted()
    {
        return $this->dateStarted;
    }

    /**
     * Set dateFinished
     *
     * @param \DateTime $dateFinished
     * @return Company
     */
    public function setDateFinished($dateFinished)
    {
        $this->dateFinished = $dateFinished;

        return $this;
    }

    /**
     * Get dateFinished
     *
     * @return \DateTime 
     */
    public function getDateFinished()
    {
        return $this->dateFinished;
    }

    /**
     * Set isCurrent
     *
     * @param boolean $isCurrent
     * @return Company
     */
    public function setIsCurrent($isCurrent)
    {
        $this->isCurrent = $isCurrent;

        return $this;
    }

    /**
     * Get isCurrent
     *
     * @return boolean 
     */
    public function getIsCurrent()
    {
        return $this->isCurrent;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Company
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
