<?php 


namespace TestBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('jobTitle');
        $builder->add('companyName');
        $builder->add('dateStarted', DateType::class, array(
            'widget' => 'single_text',
            'format' => 'dd-MM-yyyy',
            
            'attr' => array(
              'class' => 'datepicker',
            )
        ));
        $builder->add('dateFinished', DateType::class, array(
            'widget' => 'single_text',
            'format' => 'dd-MM-yyyy',
            
            'attr' => array(
              'class' => 'datepicker',
            ),
            'required' => false,
        ));
        $builder->add('location', 'text', array(
            'attr'=> array(
                'class'=>'autocomplete-input js-location'
            )
        ));
        $builder->add('description', 'textarea', array(
            'attr'=> array(
                'class'=>'js-wysiwyg'
            ),
            'required' => false,
        ));
        $builder->add('isCurrent', 'checkbox', array(
            'label'    => 'This is my current job',
            'required' => false,
            'attr' => array(
              'class' => 'js-disable-input',
            ),
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Company',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'namecompany';
    }
}