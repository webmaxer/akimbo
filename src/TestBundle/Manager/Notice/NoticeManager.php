<?php

namespace TestBundle\Manager\Notice;

use TestBundle\Manager\Notice\NoticeEmail;

class NoticeManager
{
	/**
	 * The E-mail notice
	 *
	 * @var \TestBundle\Manager\Notice\NoticeInterface
	*/
	private $noticeEmail;

	/**
	 * Constuctor
	 *
	 * @param \TestBundle\Manager\Notice\NoticeInterface $noticeEmail
	*/
	public function __construct(NoticeInterface $noticeEmail)
	{
		$this->noticeEmail = $noticeEmail;
	}

	/**
	 * Notify at E-mail of registration
	 *
	 * @param string $subject
	 * @param string $to
	 * @param string $template
	 * @param array $templateData
	 */
	public function notifyAtEmailOfRegistration($to, array $templateData = [])
	{
		//add message images
		$templateData[NoticeEmail::MESSAGE_IMAGES] = [
			'logo' => '/../Resources/views/Emails/akimbo_logo.png',
      		'f'    => '/../Resources/views/Emails/f.png',
      		't'    => '/../Resources/views/Emails/t.png',
      		'm'    => '/../Resources/views/Emails/m.png',
      		'in'   => '/../Resources/views/Emails/in.png',
		];

		$this->noticeEmail->createMessageByTemplate('TestBundle:Emails:registration', $templateData);
		$this->noticeEmail->send([
			NoticeEmail::MESSAGE_TO      => $to,
			NoticeEmail::MESSAGE_SUBJECT => 'Hello Email',
		]);
	}
}