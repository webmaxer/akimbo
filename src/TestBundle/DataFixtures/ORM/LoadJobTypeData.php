<?php

namespace TestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TestBundle\Entity\JobType;

class LoadJobTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $types = ['Full Time', 'Part Time', 'Shift', 'Seasonal', 'Job-sharing', 'Intership',
            'Volunteer', 'On-call', 'Per diem'
        ];

        foreach ($types AS $nameType) {
            $type = new JobType();
            $type->setJobTypeTitle($nameType);

            $manager->persist($type);   
        }

        $manager->flush();

        //$this->addReference('admin-user', $userAdmin);
    }

    public function getOrder()
    {
        return 1;
    }
}