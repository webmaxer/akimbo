<?php 


namespace TestBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use TestBundle\Entity\Company;
use TestBundle\Entity\Education;
use TestBundle\Entity\Reference;
use TestBundle\Entity\Industry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    public function __construct($em) {
        $this->em = $em;
   }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', 'text', array(
            'error_bubbling' => true
        ));
        $builder->add('lastName', 'text', array(
            'error_bubbling' => true
        ));
        $builder->add('linkedin', 'url', array(
            'error_bubbling' => true,
            'required' => false
        ));

        $builder->add('link', 'url', array(
            'error_bubbling' => true,
            'required' => false
        ));

        $builder->add('document', FileType::class, array(
            'label' => 'Brochure (PDF file)'
        ));
        $builder->add('email', 'email', array(
            'error_bubbling' => true
        ));
        $builder->add('phone', 'text', array(
            'error_bubbling' => true,
            'attr' => array(
                'placeholder' => 'Phone number'
            )
        ));
        $builder->add('location', 'text', array(
            'error_bubbling' => true,
            'attr'=> array(
                'placeholder' => 'City, state'
            ),
        ));
        $builder->add('goToPreview', 'submit', array(
            'label' => 'Preview Resume',
            'attr' => array(
                'class'=>'btn waves-effect waves-light right',
            ),
        ));
        // $builder->add('password', 'repeated', array(
        //     'type' => 'password',
        //     'invalid_message' => 'The password fields must match.',
        //     'options' => array('attr' => array('class' => 'password-field')),
        //     'required' => true,
        //     'first_options'  => array('label' => 'Password'),
        //     'second_options' => array('label' => 'Repeat Password'),
        //     'error_bubbling' => true,
        // ));
        $builder->add('description', 'textarea', array(
            'error_bubbling' => true,
            'data' => ($options['data']->getDescription()!='') ? $options['data']->getDescription() : "<p>The great thing about <strong>MY INDUSTRY</strong> is that it's always improving and making new strides. With over <strong>YEARS </strong>of experience in this industry, my passion has yet to diminish. During my tenure, I've seen this sector adapt and become innovative in this digital world. I'm eager to merge my expertise with your organization's mission, where together we can take things to the next level.</p>",
            'label' => false
        ));

        $builder->add('skills', 'entity', array(
            'class' => 'TestBundle:Skill',
            'property' => 'skillName',
            'expanded' => false,
            'multiple' => true,
            'required' => false,
            'error_bubbling' => true
        ));

        $builder->add('certificates', 'entity', array(
            'class' => 'TestBundle:Certificate',
            'property' => 'certificateName',
            'expanded' => false,
            'multiple' => true,
            'required' => false,
            'error_bubbling' => true
        ));

        $builder->add('companies', 'collection', array(
            'type' => new CompanyType(),
            'allow_add' => true,
            'delete_empty' => true,
            'prototype' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'label'=> 'Work experience',
            'options' => array(
                        'label'=> false,
                    )
        ));

        $builder->add('educations', 'collection', array(
            'type' => new EducationType(),
            'allow_add' => true,
            'delete_empty' => true,
            'prototype' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'label'=> 'Education',
            'options' => array(
                        'label'=> false,
                    )
        ));

        $builder->add('references', 'collection', array(
            'type' => new ReferenceType(),
            'allow_add' => true,
            'delete_empty' => true,
            'prototype' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'label'=> 'References',
            'options' => array(
                        'label'=> false,
                    )
        ));

        $builder->add('industry', 'entity', array(
            'class' => 'TestBundle:Industry',
            'property' => 'industryName',
            'expanded' => false,
            'multiple' => false,
            'required' => false,
            'error_bubbling' => true,
        ));


        $builder->add('industry', 'entity', array(
            'class' => 'TestBundle:Industry',
            'property' => 'industryName',
            'expanded' => false,
            'multiple' => false,
            'required' => false,
            'error_bubbling' => true,
        ));

        $builder->add('jobPreference', new JobPreferenceType($this->em));






    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\User',
            'validation_groups' => function (FormInterface $form) {
                if ($form->get('goToPreview')->isClicked()) {
                    return array('Default', 'registration');
                } 
                else {
                    return array('update', 'registration');
                }
            },
                       
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'resumeform';
    }
}