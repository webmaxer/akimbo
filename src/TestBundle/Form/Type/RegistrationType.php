<?php 


namespace TestBundle\Form\Type;

use TestBundle\Entity\Industry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{

    public function __construct($em) {
        $this->em = $em;
   }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', 'text', array(
            'error_bubbling' => true
        ));
        $builder->add('lastName', 'text', array(
            'error_bubbling' => true
        ));

        $builder->add('email', 'email', array(
            'error_bubbling' => true
        ));

        $builder->add('password', 'password', array(
            'error_bubbling' => true,
        ));




        $builder->add('industry', 'entity', array(
            'class' => 'TestBundle:Industry',
            'property' => 'industryName',
            'error_bubbling' => true,
            'data' => $this->em->getReference("TestBundle:Industry", 1),
            'attr'=> array(
                'class'=>'autocomplete'
            ),
        ));

        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\User',
            'validation_groups' => array('registration'),
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'resumeform';
    }
}