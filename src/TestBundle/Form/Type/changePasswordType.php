<?php 


namespace TestBundle\Form\Type;

use TestBundle\Entity\Industry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class changePasswordType extends AbstractType
{

    public function __construct($em) {
        $this->em = $em;
   }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('password', 'password', array(
            'error_bubbling' => true,
        ));






        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\User',
            'validation_groups' => array('changePassword'),
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'changePassword';
    }
}