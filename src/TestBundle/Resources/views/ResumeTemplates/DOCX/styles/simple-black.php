<?php

use PhpOffice\PhpWord\Style\Font;

// Example styles
// This is an example of how a "style sheet" should be structured
// to asign PHPWord styles to HTML elements, classes, and inline
// styles.
function htmltodocx_styles_example()
{

    $paper_gutter = 2;
    $paper_width_inch = 8.5;
    $paper_with_gutter = $paper_width_inch+$paper_gutter;
    $pt_in_inch = 75;
    $page_width = $paper_with_gutter * $pt_in_inch;
    $gutter = 8*50;
    $twip = 20;
    $ptw = $twip * $page_width;
    // Set of default styles -
    // to set initially whatever the element is:
    // NB - any defaults not set here will be provided by PHPWord.
    $styles['default'] =
        array(
            'size' => 14,
            'name' => 'Times New Roman',

        );

    // Element styles:
    // The keys of the elements array are valid HTML tags;
    // The arrays associated with each of these tags is a set
    // of PHPWord style definitions.
    $styles['elements'] =
        array(
            'h1'     => array(
                'bold' => TRUE,
                'size' => 20,
            ),
            'h2'     => array(
                'bold'       => TRUE,
                'size'       => 15,
                'spaceAfter' => 150,
            ),
            'h3'     => array(
                'size'       => 12,
                'bold'       => TRUE,
                'spaceAfter' => 100,
            ),
            'p'     => [
                'spaceAfter' => 0,
                'spaceBefore' => 0,
                'lineHeight' => 1.5
            ],
            'ol'     => array(
                'spaceBefore' => 200,
            ),
            'ul'     => array(
                'spaceAfter' => 150,
            ),
            'b'      => array(
                'bold' => TRUE,
            ),
            'em'     => array(
                'italic' => TRUE,
            ),
            'i'      => array(
                'italic' => TRUE,
            ),
            'strong' => array(
                'bold' => TRUE,
            ),
            'b'      => array(
                'bold' => TRUE,
            ),
            'sup'    => array(
                'superScript' => TRUE,
                'size'        => 6,
            ), // Superscript not working in PHPWord
            'u'      => array(
                'underline' => Font::UNDERLINE_SINGLE,
            ),
            'a'      => array(
                'color'     => '0000FF',
                'underline' => Font::UNDERLINE_SINGLE,
            ),
            'table'  => array(
                // Note that applying a table style in PHPWord applies the relevant style to
                // ALL the cells in the table. So, for example, the borderSize applied here
                // applies to all the cells, and not just to the outer edges of the table:

            ),
            'tr'     => array(

            ),
            'td'     => array(

            ),
            'hr'     => array(
                'height' => 0,
                'width' => 600, 
                'spaceAfter'=>0,
                'spaceBefore'=>0,
                // weight. Line width in twips.
                // color. Defines the color of stroke.
                // dash. Line types: dash, rounddot, squaredot, dashdot, longdash, longdashdot, longdashdotdot.
                // beginArrow. Start type of arrow: block, open, classic, diamond, oval.
                // endArrow. End type of arrow: block, open, classic, diamond, oval.
                // width. Line-object width in pt.
                // height. Line-object height in pt.
                // flip. Flip the line element: true, false.
            ),
            'li' => [
                'spaceAfter'=>0,
                'lineHeight'=>1.5,
                'indent' => 1
                // 'indentation' => 2,
                // 'left' => 2,
                // 'hanging' => 2,
            ]
        );

    // Classes:
    // The keys of the classes array are valid CSS classes;
    // The array associated with each of these classes is a set
    // of PHPWord style definitions.
    // Classes will be applied in the order that they appear here if
    // more than one class appears on an element.
    $styles['classes'] =
    [
        'container' => [
            'width' => 100*50, 
            'unit' => 'pct', 
        ],
        'header' => [

        ],
        'header-title' => [
            'size' => 20,
            'spaceAfter'=>0
        ],

        'align-right' => [
            'align' => 'right',
        ],
        'align-center' => [
            'align' => 'center',
        ],        
        'list-arrow-shadow' => [
            'name' => 'Wingdings',
        ],
        'field-name' => [
            'underline' => 'single',
            'bold' => true,
        ],
        'field-name-title' => [
            'spaceAfter' => 150,
        ],

        'line-separator' => [
            'color' => '#000',
            'weight' => 1,
            'flip' => true
        ],

        'font-big' => [
            'size' => 11*1.2
        ],
        'font-small' => [
            'size' => 11*0.75
        ],        
        'list' => [
            'indent' => 1
        ],
        'list-no-indent' => [
            'indent' => 0
        ],


    ];

    // Inline style definitions, of the form:
    // array(css attribute-value - separated by a colon and a single space => array of
    // PHPWord attribute value pairs.
    $styles['inline'] =
        array(
            'text-decoration: underline' => array(
                'underline' => Font::UNDERLINE_SINGLE,
            ),
            'text-decoration: line-through' => array(
                'strikethrough' => true,
            ),

            'vertical-align: left'       => array(
                'align' => 'left',
            ),
            'vertical-align: middle'     => array(
                'align' => 'center',
            ),
            'vertical-align: right'      => array(
                'align' => 'right',
            ),

            'text-align: center'      => array(
                'align' => 'center',
            ),

            'text-align: left'      => array(
                'align' => 'left',
            ),

            'text-align: right'      => array(
                'align' => 'right',
            ),

        );

    return $styles;
}