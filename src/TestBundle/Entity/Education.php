<?php

namespace TestBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Education
 *
 * @ORM\Table(name="education")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\EducationRepository")
 */
class Education
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank( message = "Institute name should not be blank" )
     * @ORM\Column(name="institute_name", type="string", length=255)
     */
    private $instituteName;

    /**
     * @var string
     * @Assert\NotBlank( message = "Degree should not be blank" )
     * @ORM\Column(name="degree", type="string")
     */
    private $degree;

    /**
     * @var \Date
     * @Assert\LessThanOrEqual("now")
     * @ORM\Column(name="date_finished", type="date", nullable=true)
     */
    private $dateFinished;

    /**
     * @var string
     * @Assert\NotBlank( message = "Location should not be blank" )
     * @ORM\Column(name="location", type="string")
     */
    private $location;



    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="educations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set yearFinished
     *
     * @param \Date $yearFinished
     * @return Education
     */
    public function setYearFinished($yearFinished)
    {
        $this->yearFinished = $yearFinished;

        return $this;
    }

    /**
     * Get yearFinished
     *
     * @return \Date
     */
    public function getYearFinished()
    {
        return $this->yearFinished;
    }

    /**
     * Set user
     *
     * @param \TestBundle\Entity\User $user
     * @return Education
     */
    public function setUser(\TestBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TestBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set instituteName
     *
     * @param string $instituteName
     * @return Education
     */
    public function setInstituteName($instituteName)
    {
        $this->instituteName = $instituteName;

        return $this;
    }

    /**
     * Get instituteName
     *
     * @return string 
     */
    public function getInstituteName()
    {
        return $this->instituteName;
    }

    /**
     * Set degree
     *
     * @param string $degree
     * @return Education
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * Get degree
     *
     * @return string 
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Education
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set dateFinished
     *
     * @param \DateTime $dateFinished
     * @return Education
     */
    public function setDateFinished($dateFinished)
    {
        $this->dateFinished = $dateFinished;

        return $this;
    }

    /**
     * Get dateFinished
     *
     * @return \DateTime 
     */
    public function getDateFinished()
    {
        return $this->dateFinished;
    }
}
