<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Certificate
 *
 * @ORM\Table(name="certificate")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\CertificateRepository")
 */
class Certificate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="certificate_name", type="string", length=255)
     */
    private $certificateName;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="certificates")
     */
    private $users;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set certificateName
     *
     * @param string $certificateName
     * @return Certificate
     */
    public function setCertificateName($certificateName)
    {
        $this->certificateName = $certificateName;

        return $this;
    }

    /**
     * Get certificateName
     *
     * @return string 
     */
    public function getCertificateName()
    {
        return $this->certificateName;
    }

    /**
     * Add users
     *
     * @param \TestBundle\Entity\User $users
     * @return Certificate
     */
    public function addUser(\TestBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \TestBundle\Entity\User $users
     */
    public function removeUser(\TestBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
}
