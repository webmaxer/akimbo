<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\Debug\ErrorHandler;
use AdminBundle\Entity\Admin;
use TestBundle\Form\Type\AdminUpdateType;

class DefaultController extends Controller
{
    /**
     * @Route("/admin/dashboard/", name="dashboard")
     */
    public function dashboardAction(Request $request)
    {   
      return $this->render('AdminBundle:Default:dashboard.html.twig');
    }

    /**
     * @Route("/admin/createAdmin/", name="createAdmin")
     */
    public function createAdminAction(Request $request)
    {   
        try {
          $email = $request->request->get('na_email');
          $password = $request->request->get('na_password');
          $name = $request->request->get('na_name');
          $em = $this->getDoctrine()->getManager();
          $admin = new Admin();
          $admin->setEmail($email);
          $admin->setPassword($password);
          $admin->setName($name);

          $em->persist($admin);
          $em->flush();
          $response = array("code" => 500, "success" => true);
        } catch (\Exception $e) {
          $handler = new ExceptionHandler();
          $handler->handle( $e );

          $response = array("code" => 500, "success" => false, "message"=>$e->getMessage());
        }        

        return new Response(json_encode($response));
        
    }



    private function getSkillsArray($skills) {
      $data = [];
      foreach ($skills as $skill) {
        $data[]['skillName'] = $skill->getSkillName();
      }
      return $data;
    }

    private function getCertificatesArray($certificates) {
      $data = [];
      foreach ($certificates as $certificate) {
        $data[]['certificateName'] = $certificate->getCertificateName();
      }
      return $data;
    }

    /**
     * @Route("/api/users", name="api.users")
     */
    public function apiUsersAction(Request $request)
    {   

      $sort = $request->query->get('sort');
      list($field, $order) = explode('|', $sort);
      $filter = $request->query->get('filter');

      $em = $this->getDoctrine()->getManager();
      $users = $em->getRepository('TestBundle:User')->findAll($field, $order, $filter);
      $response['data'] = [];
      foreach ($users as $user) {
          $response['data'][] = [
              'id' => $user->getId(),
              'fullname' => $user->getFirstName().' '.$user->getLastName(),
              'location' => $user->getLocation(),
              'email' => $user->getEmail(),
              'phone' => $user->getPhone(),
              'industry' => $user->getIndustry()->getIndustryName(),
              'industryImg' => $user->getIndustry()->getIndustryImage(),
              'skills' => $this->getSkillsArray( $user->getSkills() ),
              'certificates' => $this->getCertificatesArray( $user->getCertificates() ),
              'confirmed' => $user->getIsConfirmed(),
              'status' => $user->getStatus(),
              'createdAt' => $user->getCreatedAt(),
          ];
      }
      $response['total'] = sizeof($users);
      $response['per_page'] = sizeof($users);
      $response['current_page'] = 1;
      $response['last_page'] = 1;
      $response['next_page_url'] = "\/api\/users";
      $response['prev_page_url'] = "null";
      $response['from'] = "1";
      $response['to'] = sizeof($users);

      try {
        return new Response(json_encode($response));
      } catch (Exception $e) {
        return new Response($e);
      }
        
    }



    /**
     * @Route("/api/user/{id}", name="api.userAction")
     */
    public function apiUserAction(Request $request, $id=null)
    {   

      if (!$request->isXmlHttpRequest()) {
        return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
      }
      if ($id == null) {
        return new JsonResponse(array('message' => "User with id: $id not found"), 400);
      }

      $em = $this->getDoctrine()->getManager();
      $user = $em->getRepository('TestBundle:User')->find($id);

      // Update user
      if ($request->isMethod('POST')) {
          $form = $this->createForm(new AdminUpdateType($em), $user);
          $form->handleRequest($request);
          $html = $this->renderView('AdminBundle:Forms:user-update.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
          ]);
          if ($form->isValid()) {
              $em->persist($user);
              $em->flush();
              return new JsonResponse(['message' => 'Success!', 'html' => $html, 'image'=>$user->getIndustry()->getIndustryImage(), 200]);
          }
          else {
            return new JsonResponse(['message' => 'Form rendered successfully, but not valid', 'html' => $html, 200 ]);
          }
      }
      // End update user

      // Delete user
      elseif ($request->isMethod('DELETE')) {
        if ($user != null) {
          try {
            $em->remove($user);
            $em->flush();
            return new JsonResponse(array('message' => "User with id: $id successfully deleted"), 200);
          } catch (Exception $e) {
            return new JsonResponse(array('message' => "User with id: $id not deleted, because system error occured. <br>$e"), 400);
          }
        }
        else {
          return new JsonResponse(array('message' => "User with id: $id not found"), 400);
        }
      }
      // End delete user

      else {
        return new Response('Invalid request');
      }


    }

    /**
     * @Route("/admin/jobs/", name="admin_jobs")
     */
    public function jobsAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        return $this->render('AdminBundle:Default:jobs.html.twig', [
            'types'      => $em->getRepository('TestBundle:JobType')->findAll(),
            'industries' => $em->getRepository('TestBundle:Industry')->findAll(),
        ]);
    }


    /**
     * @Route("/admin/jobs/new", name="api.jobs")
     */
    public function apiJobsAction(Request $request)
    {

        $em = $this->get('doctrine.orm.entity_manager');

        if ($request->getMethod() == 'POST') {
            $em->getRepository('TestBundle:Job')->add($request->get('job'));

            return new JsonResponse([
                'status' => 'ok',
            ]);
        } else {
            return new JsonResponse(array(
                'jobs' => $this->get('app.serializer')->serializeJobs($em->getRepository('TestBundle:Job')->findBy([], ['id' => "DESC"]))
            ));
        }
    }
}
