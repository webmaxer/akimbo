<?php 

// src/TestBundle/Twig/AppExtension.php
namespace TestBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('read_file', array($this, 'readFileFilter')),
        );
    }

    public function readFileFilter($path)
    {
        return file_get_contents($path);
    }

    public function getName()
    {
        return 'app_extension';
    }
}