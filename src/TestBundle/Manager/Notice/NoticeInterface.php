<?php

namespace TestBundle\Manager\Notice;

interface NoticeInterface
{
	/**
	 * Send message
	 *
	 * @param array $dataMessage
	*/
	public function send(array $dataMessage);
}