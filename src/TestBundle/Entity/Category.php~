<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="category_name", type="string", length=255)
     */
    private $categoryName;

    /**
     * @var string
     *
     * @ORM\Column(name="category_image", type="string", length=255)
     */
    private $categoryImage;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="category")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="JobPreference", mappedBy="category")
     */
    private $JobPreferences;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Add users
     *
     * @param \TestBundle\Entity\User $users
     * @return Certificate
     */
    public function addUser(\TestBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \TestBundle\Entity\User $users
     */
    public function removeUser(\TestBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     * @return Category
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    /**
     * Get categoryName
     *
     * @return string 
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * Add JobPreferences
     *
     * @param \TestBundle\Entity\JobPreference $jobPreferences
     * @return Category
     */
    public function addJobPreference(\TestBundle\Entity\JobPreference $jobPreferences)
    {
        $this->JobPreferences[] = $jobPreferences;

        return $this;
    }

    /**
     * Remove JobPreferences
     *
     * @param \TestBundle\Entity\JobPreference $jobPreferences
     */
    public function removeJobPreference(\TestBundle\Entity\JobPreference $jobPreferences)
    {
        $this->JobPreferences->removeElement($jobPreferences);
    }

    /**
     * Get JobPreferences
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobPreferences()
    {
        return $this->JobPreferences;
    }

    /**
     * Set categoryImage
     *
     * @param string $categoryImage
     * @return Category
     */
    public function setCategoryImage($categoryImage)
    {
        $this->categoryImage = $categoryImage;

        return $this;
    }

    /**
     * Get categoryImage
     *
     * @return string 
     */
    public function getCategoryImage()
    {
        return $this->categoryImage;
    }
}
