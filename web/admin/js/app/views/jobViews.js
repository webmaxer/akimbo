var JobDetailView = Backbone.View.extend({
    template: $("#joblistTemplate").html(),
    tagName:   "tr",

    events: {
        "click .edit": "editJob",
    },

   /* removeReview: function(event) {
    	if (confirm('Удалить отзыв?')) {
    	    this.model.destroy({ success:this.removeReviewDom(this), wait:true });
    	}
    },*/

    editJob: function(event) {

        $('#wrapper_popup').addClass('visible');
        $('#wrapper_popup').addClass('active');
        $('#edit_form_job').addClass('visible');
        $('#edit_form_job').addClass('active');
    	
        $('#edit_form_job').attr('data-id', this.model.get('id'));
        $('#edit_jobTitle').val(this.model.get('jobTitle'));
        $('#edit_salary').val(this.model.get('salary'));
        $('#edit_description').val(this.model.get('description'));
        $('#edit_location').val(this.model.get('location'));
    	$('#edit_company option[value="'+this.model.get('company').companyName+'"]').prop({'selected':true});

        var industries = this.model.get('industries');
        var types      = this.model.get('jobTypes');

        for (i in industries) {
            var industry = industries[i];

            $('#edit_form_job [name="edit_industries"][value="'+industry.id+'"]').prop({'checked':true});
        }

        for (k in types) {
            var type = types[k];

            $('#edit_form_job [name="edit_types"][value="'+type.id+'"]').prop({'checked':true});
        }
    },
	    
    render: function () {
        var tmp = _.template(this.template);

        this.$el.html(tmp(this.model.toJSON()));

		return this;
    }
 });

var JobsListView = Backbone.View.extend({
    el: $("#jobsList"),

    events: {
        'click [bind="add_job"]': "addJob",
    },

    initialize: function() {
	    this.collection.on('add', this.renderJob, this);
	    //this.collection.on('sync', this.renderPagination, this);
    },

    clearList: function() {
    	this.$el.find('tbody').html('');
    },

    render: function () {
        this.collection.each(this.renderJob, this);
        return this;
    },

    renderJob: function (job) {
        var jobDetailView = new JobDetailView({ model: job });
        this.$el.append(jobDetailView.render().el);
    },

    addJob: function(event) {
    	$('#wrapper_popup').addClass('visible');
    	$('#wrapper_popup').addClass('active');
    	$('#add_form_job').addClass('visible');
    	$('#add_form_job').addClass('active');
    }

	/*renderPagination: function(collection) {
		paginationView.render(collection);
	}*/
});

/*var PaginationView = Backbone.View.extend({
	    el: $("#pagination"),
	    template: $("#paginationTemplate").html(),

	    events: {
            "click a": "getPage",
        },

	    initialize: function() {
		    //
	    },

	    render: function (collection) {

	    	var tmp  = _.template(this.template);
	    	var page = collection.page;

	    	this.$el.html('');

	    	for (i=1; i<page.count; i++) {
	    		if (i == page.current) {
	    			var class_page = 'current';
	    		} else {
	    			var class_page = 'page';
	    		}

	    		this.$el.append(tmp({ class_page:class_page, num_page:i }));
	    	}
	    },

	    getPage: function(event) {
	    	var target = event.currentTarget;
	    	var page   = Number($(target).text());

	    	reviewsView.clearList();
	    	reviewsCollection.reset();

	    	reviewsCollection.fetch({ data: {'type':'check', 'page':page } });
	    }
	});*/