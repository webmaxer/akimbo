<?php 
namespace TestBundle\Utils\DocxGenerator;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use TestBundle\Utils\DocxGenerator\lib\HtmlToDocx\HtmlToDocx;


class DocxGenerator 
{

  private $html;
  private $options;
  private $styles;

  public function __construct($html, $options, $styles) {
    $this->html = $html;
    $this->options = $options;
    $this->styles = $styles;
  }

  public function generateDocx($file) {
    $htmldocx = new HtmlToDocx();
    // HTML fragment we want to parse:
    
    // $html = file_get_contents('test/table.html');
     
    // New Word Document
    $phpword_object = new PhpWord();
    $section = $phpword_object->createSection($this->options);

    // HTML Dom object:
    $html_dom = $htmldocx->getHtml('<html><body>' . $this->html . '</body></html>');
    // Note, we needed to nest the html in a couple of dummy elements.

    // Create the dom array of elements which we are going to work on:
    $html_dom_array = $html_dom->find('html',0)->children();

    // We need this for setting base_root and base_path in the initial_state array
    // (below). We are using a function here (derived from Drupal) to create these
    // paths automatically - you may want to do something different in your
    // implementation. This function is in the included file 
    // documentation/support_functions.inc.
    $paths = $this->htmltodocx_paths();
    require $this->styles;
    // Provide some initial settings:
    $initial_state = array(
      // Required parameters:
      'phpword_object' => &$phpword_object, // Must be passed by reference.
      // 'base_root' => 'http://test.local', // Required for link elements - change it to your domain.
      // 'base_path' => '/htmltodocx/documentation/', // Path from base_root to whatever url your links are relative to.
      'base_root' => $paths['base_root'],
      'base_path' => $paths['base_path'],
      // Optional parameters - showing the defaults if you don't set anything:
      'current_style' => array('size' => 11), // The PHPWord style on the top element - may be inherited by descendent elements.
      'parents' => array(0 => 'body'), // Our parent is body.
      'list_depth' => 0, // This is the current depth of any current list.
      'context' => 'section', // Possible values - section, footer or header.
      'pseudo_list' => TRUE, // NOTE: Word lists not yet supported (TRUE is the only option at present).
      'pseudo_list_indicator_font_name' => 'Wingdings', // Bullet indicator font.
      'pseudo_list_indicator_font_size' => '7', // Bullet indicator size.
      'pseudo_list_indicator_character' => 'l ', // Gives a circle bullet point with wingdings.
      'table_allowed' => TRUE, // Note, if you are adding this html into a PHPWord table you should set this to FALSE: tables cannot be nested in PHPWord.
      'treat_div_as_paragraph' => TRUE, // If set to TRUE, each new div will trigger a new line in the Word document.
          
      // Optional - no default:    
      'style_sheet' => htmltodocx_styles_example(), // This is an array (the "style sheet") - returned by htmltodocx_styles_example() here (in styles.inc) - see this function for an example of how to construct this array.
      );    

    // Convert the HTML and put it into the PHPWord object
    $htmldocx->insertHtml($section, $html_dom_array[0]->nodes, $initial_state);

    // Clear the HTML dom object:
    $html_dom->clear(); 
    unset($html_dom);
    // Save File
    $objWriter = IOFactory::createWriter($phpword_object, 'Word2007');
    $objWriter->save($file);
  }



  /**
   * Computes base root, base path, and base url.
   * 
   * This code is adapted from Drupal function conf_init, see:
   * http://api.drupal.org/api/drupal/includes%21bootstrap.inc/function/conf_init/6
   * 
   */
  private function htmltodocx_paths() {
    
    if (!isset($_SERVER['SERVER_PROTOCOL']) || ($_SERVER['SERVER_PROTOCOL'] != 'HTTP/1.0' && $_SERVER['SERVER_PROTOCOL'] != 'HTTP/1.1')) {
      $_SERVER['SERVER_PROTOCOL'] = 'HTTP/1.0';
    }

    if (isset($_SERVER['HTTP_HOST'])) {
      // As HTTP_HOST is user input, ensure it only contains characters allowed
      // in hostnames. See RFC 952 (and RFC 2181).
      // $_SERVER['HTTP_HOST'] is lowercased here per specifications.
      $_SERVER['HTTP_HOST'] = strtolower($_SERVER['HTTP_HOST']);
      if (!$this->htmltodocx_valid_http_host($_SERVER['HTTP_HOST'])) {
        // HTTP_HOST is invalid, e.g. if containing slashes it may be an attack.
        header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request');
        exit;
      }
    }
    else {
      // Some pre-HTTP/1.1 clients will not send a Host header. Ensure the key is
      // defined for E_ALL compliance.
      $_SERVER['HTTP_HOST'] = '';
    }

    // Create base URL
    $base_root = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';

    $base_url = $base_root .= '://' . $_SERVER['HTTP_HOST'];

    // $_SERVER['SCRIPT_NAME'] can, in contrast to $_SERVER['PHP_SELF'], not
    // be modified by a visitor.
    if ($dir = trim(dirname($_SERVER['SCRIPT_NAME']), '\,/')) {
      $base_path = "/$dir";
      $base_url .= $base_path;
      $base_path .= '/';
    }
    else {
      $base_path = '/';
    }
    
    return array(
      'base_path' => $base_path,
      'base_url' => $base_url,
      'base_root' => $base_root,
    );
    
  }

  /**
   * Check for valid http host.
   * 
   * This code is adapted from function drupal_valid_http_host, see:
   * http://api.drupal.org/api/drupal/includes%21bootstrap.inc/function/drupal_valid_http_host/6
   * 
   * @param mixed $host
   * @return int
   */
  private function htmltodocx_valid_http_host($host) {
    return preg_match('/^\[?(?:[a-z0-9-:\]_]+\.?)+$/', $host);
  }
}