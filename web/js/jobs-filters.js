$(document).ready(function(){
    
    var months = {
        0: 'Jun',
        1: 'Feb',
        2: 'Mar',
        3: 'Apr',
        4: 'May',
        5: 'June',
        6: 'July',
        7: 'Aug',
        8: 'Sept',
        9: 'Oct',
        10: 'Nov',
        11: 'Dec',
    };
    
    //filters jobs
    function do_filters_job() {
	//defined vars
        var filters = {};
	var industries = [];
	var schedule    = [];
        
        //add industries
        $('[filter="industry"]').find('[type="checkbox"]:checked').each(function(){
            industries.push($(this).val());
	});

	//schedule
        $('[filter="schedule"]').find('[type="checkbox"]:checked').each(function(){
            schedule.push($(this).val());
	});

	//add to filters
        filters.posted     = $('[filter="post_salary"]').find('select[name="posted"]').val();
	filters.salary     = $('[filter="post_salary"]').find('select[name="salary"]').val();
	filters.sort       = $('[filter="sort"]').find('select[name="sort"]').val();
	filters.title      = $('[name="title"]').val();
	filters.location   = $('[name="location"]').val();
	filters.schedule   = schedule;
	filters.industries = industries;
        
        $('.job-list').prepend('<div class="preloader"></div>');

	//make request
        $.ajax({
            url: window.location+'/filters/',
            type: 'POST',
            dataType: 'JSON',
            data: { filters: filters },
            success: function(data) {
		$('.preloader').remove();
                render_html($.parseJSON(data.jobs));
            }
	});
    }
    
    function render_html(jobs) {
        
        $('.job-list').html('');
        
        for (k in jobs) {
            
            var job  = jobs[k];
            var date = new Date(job.created.timestamp * 1000);
            
            $('.job-list').append('<div class="item">'+
                '<div class="card-content">'+
                    '<div class="left-part">'+
                        '<h4 class="job-title"><a href="#">'+job.jobTitle+'</a></h4>'+
                        '<p class="job-ad">'+job.description+'</p>'+
                        '<div><a href="#" class="job-readmore">Read more &rarr;</a></div>'+
                        '<div class="job-footer">'+
                           '<span class="job-date">'+months[date.getMonth()]+' '+date.getDay()+', '+date.getFullYear()+'</span>'+
                           '<div class="job-location"><i class="material-icons">location_on</i>'+job.location+'</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="right-part">'+
                        '<div class="job-salary">$'+job.salary+'+</div>'+
                        '<a href="#"><img src="https://static1.squarespace.com/static/53863c3fe4b0643327cbaaa1/t/57d8674be4fcb5360f2ca4d2/1473800018145/?format=300w" alt="" class="job-image"></a>'+
                    '</div>'+              
                '</div>'+
            '</div>');
        }
    }

    $('select').change(do_filters_job);
    $('[type="checkbox"]').click(do_filters_job);
    $('#search-jobs').click(do_filters_job);
});