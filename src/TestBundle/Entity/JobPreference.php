<?php

namespace TestBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * JobPreference
 *
 * @ORM\Table(name="job_preference")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\JobPreferenceRepository")
 */
class JobPreference
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="idealJobTitle", type="string", nullable=true)
     */
    private $idealJobTitle = null;

    /**
     * @var string
     * @ORM\Column(name="location", type="string", nullable=true)
     */
    private $location = null;

    /**
     * @var string
     * @ORM\Column(name="salary", type="integer", nullable=true)
     */
    private $salary = null;
    
    /**
     * @ORM\ManyToMany(targetEntity="JobType", inversedBy="jobPreferences")
     * @ORM\JoinTable(name="jobPreferences_jobTypes",
     *      joinColumns={@ORM\JoinColumn(name="jobPreference_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="jobType_id", referencedColumnName="id")}
     *      )
     */
    private $jobTypes;


    /**
     * @ORM\ManyToMany(targetEntity="Industry", inversedBy="jobPreferences")
     * @ORM\JoinTable(name="jobPreferences_Industries",
     *      joinColumns={@ORM\JoinColumn(name="jobPreference_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="industry_id", referencedColumnName="id")}
     *      )
     */
    private $industries;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="jobPreference")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jobTypes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->industries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idealJobTitle
     *
     * @param string $idealJobTitle
     * @return JobPreference
     */
    public function setIdealJobTitle($idealJobTitle)
    {
        $this->idealJobTitle = $idealJobTitle;

        return $this;
    }

    /**
     * Get idealJobTitle
     *
     * @return string 
     */
    public function getIdealJobTitle()
    {
        return $this->idealJobTitle;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return JobPreference
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set jobType
     *
     * @param \TestBundle\Entity\JobType $jobType
     * @return JobPreference
     */
    public function setJobType(\TestBundle\Entity\JobType $jobType = null)
    {
        $this->jobType = $jobType;

        return $this;
    }

    /**
     * Get jobType
     *
     * @return \TestBundle\Entity\JobType 
     */
    public function getJobType()
    {
        return $this->jobType;
    }

    

    /**
     * Set user
     *
     * @param \TestBundle\Entity\User $user
     * @return JobPreference
     */
    public function setUser(\TestBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TestBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add jobTypes
     *
     * @param \TestBundle\Entity\JobType $jobTypes
     * @return JobPreference
     */
    public function addJobType(\TestBundle\Entity\JobType $jobTypes)
    {
        $this->jobTypes[] = $jobTypes;

        return $this;
    }

    /**
     * Remove jobTypes
     *
     * @param \TestBundle\Entity\JobType $jobTypes
     */
    public function removeJobType(\TestBundle\Entity\JobType $jobTypes)
    {
        $this->jobTypes->removeElement($jobTypes);
    }

    /**
     * Get jobTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobTypes()
    {
        return $this->jobTypes;
    }

    /**
     * Add industries
     *
     * @param \TestBundle\Entity\Industry $industries
     * @return JobPreference
     */
    public function addIndustry(\TestBundle\Entity\Industry $industries)
    {
        $this->industries[] = $industries;

        return $this;
    }

    /**
     * Remove industries
     *
     * @param \TestBundle\Entity\Industry $industries
     */
    public function removeIndustry(\TestBundle\Entity\Industry $industries)
    {
        $this->industries->removeElement($industries);
    }

    /**
     * Get industries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIndustries()
    {
        return $this->industries;
    }

    /**
     * Set salary
     *
     * @param integer $salary
     * @return JobPreference
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return integer 
     */
    public function getSalary()
    {
        return $this->salary;
    }
}
