<?php

namespace TestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TestBundle\Entity\Job;
use TestBundle\Entity\Company;

class LoadJobData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $job1 = new Job();
        $job2 = new Job();
        $job3 = new Job();
        $job4 = new Job();

        $company1 = new Company();
        $company2 = new Company();

        $company1->setJobTitle('information-technology')
                 ->setCompanyName('Akimbo')
                 ->setLocation('New York, New York')
                 ->setDateStarted(new \Datetime(date('d.m.Y H:i:s', time())));

        $company2->setJobTitle('accounting')
                 ->setCompanyName('Crowdvis')
                 ->setLocation('New York, New York')
                 ->setDateStarted(new \Datetime(date('d.m.Y H:i:s', time())));

        $job1->setJobTitle('Branding + Marketing Intern')
             ->setSalary(20)
             ->setCompany($company1)
             ->setCreated(new \Datetime(date('d.m.Y H:i:s', time())))
             ->setLocation('New York, New York')
             ->setDescription('NYC startup Akimbo is currently hiring a brand intern for the Fall semester. Akimbo is a job-matching service specifically for diverse candidates, including LGBT individuals, people of color, and women.');

        $job1->addJobType(
            $manager->getRepository('TestBundle:JobType')->findOneBy(['jobTypeTitle' => 'Full time'])
        );
        $job1->addJobType(
            $manager->getRepository('TestBundle:JobType')->findOneBy(['jobTypeTitle' => 'Part time'])
        );

        $job1->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'information-technology'])
        );
        $job1->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'general'])
        );
        $job1->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'manufacturing'])
        );

        //JOB TWO

        $job2->setJobTitle('Remote Assistant for the Visually Impaired')
             ->setSalary(10)
             ->setCompany($company1)
             ->setCreated(new \Datetime(date('d.m.Y H:i:s', time() - 3600)))
             ->setLocation('New York, New York')
             ->setDescription('CrowdViz is looking for a Customer Service Representative that is motivated and passionate about the role. The job description entails remotely assisting blind people via video chat with visual related tasks. Contact danielle@akimboconnect.com for more details.');

        $job2->addJobType(
            $manager->getRepository('TestBundle:JobType')->findOneBy(['jobTypeTitle' => 'Job-sharing'])
        );
        $job2->addJobType(
            $manager->getRepository('TestBundle:JobType')->findOneBy(['jobTypeTitle' => 'Intership'])
        );

        $job2->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'real-estate'])
        );
        $job2->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'administrative-support'])
        );
        $job2->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'healthcare'])
        );

        //JOB THREE

        $job3->setJobTitle('Branding + Marketing Intern2')
             ->setSalary(10)
             ->setCompany($company2)
             ->setCreated(new \Datetime(date('d.m.Y H:i:s', time() - 7200)))
             ->setLocation('New York, New York')
             ->setDescription('NYC startup Akimbo is currently hiring a brand intern for the Fall semester. Akimbo is a job-matching service specifically for diverse candidates, including LGBT individuals, people of color, and women.');

        $job3->addJobType(
            $manager->getRepository('TestBundle:JobType')->findOneBy(['jobTypeTitle' => 'Seasonal'])
        );
        $job3->addJobType(
            $manager->getRepository('TestBundle:JobType')->findOneBy(['jobTypeTitle' => 'Part time'])
        );

        $job3->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'information-technology'])
        );
        $job3->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'administrative-support'])
        );
        $job3->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'transportation-delivery'])
        );

        //JOB FOUR

        $job4->setJobTitle('Remote Assistant for the Visually Impaired2')
             ->setSalary(10)
             ->setCompany($company2)
             ->setCreated(new \Datetime(date('d.m.Y H:i:s', time() - (3600 * 5))))
             ->setLocation('New York, New York')
             ->setDescription('CrowdViz is looking for a Customer Service Representative that is motivated and passionate about the role. The job description entails remotely assisting blind people via video chat with visual related tasks. Contact danielle@akimboconnect.com for more details.');

        $job4->addJobType(
            $manager->getRepository('TestBundle:JobType')->findOneBy(['jobTypeTitle' => 'Full time'])
        );
        $job4->addJobType(
            $manager->getRepository('TestBundle:JobType')->findOneBy(['jobTypeTitle' => 'Volunteer'])
        );

        $job4->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'construction'])
        );
        $job4->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'administrative-support'])
        );
        $job4->addIndustry(
            $manager->getRepository('TestBundle:Industry')->findOneBy(['industryName' => 'retail'])
        );

        $manager->persist($job1);
        $manager->persist($job2);
        $manager->persist($job3);
        $manager->persist($job4);
        $manager->persist($company1);
        $manager->persist($company2);

        $manager->flush();

        //$this->addReference('admin-user', $userAdmin);
    }

    public function getOrder()
    {
        return 2;
    }
}