<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Industry
 *
 * @ORM\Table(name="industry")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\IndustryRepository")
 */
class Industry
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="industry_name", type="string", length=255)
     */
    private $industryName;

    /**
     * @var string
     *
     * @ORM\Column(name="industry_image", type="string", length=255)
     */
    private $industryImage;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="industry")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="JobPreference", mappedBy="industries")
     */
    private $jobPreferences;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->jobPreferences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Add users
     *
     * @param \TestBundle\Entity\User $users
     * @return Certificate
     */
    public function addUser(\TestBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \TestBundle\Entity\User $users
     */
    public function removeUser(\TestBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set industryName
     *
     * @param string $industryName
     * @return industry
     */
    public function setIndustryName($industryName)
    {
        $this->industryName = $industryName;

        return $this;
    }

    /**
     * Get industryName
     *
     * @return string 
     */
    public function getIndustryName()
    {
        return $this->industryName;
    }

    /**
     * Add JobPreferences
     *
     * @param \TestBundle\Entity\JobPreference $jobPreferences
     * @return industry
     */
    public function addJobPreference(\TestBundle\Entity\JobPreference $jobPreferences)
    {
        $this->JobPreferences[] = $jobPreferences;

        return $this;
    }

    /**
     * Remove JobPreferences
     *
     * @param \TestBundle\Entity\JobPreference $jobPreferences
     */
    public function removeJobPreference(\TestBundle\Entity\JobPreference $jobPreferences)
    {
        $this->JobPreferences->removeElement($jobPreferences);
    }

    /**
     * Get JobPreferences
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobPreferences()
    {
        return $this->JobPreferences;
    }

    /**
     * Set industryImage
     *
     * @param string $industryImage
     * @return industry
     */
    public function setIndustryImage($industryImage)
    {
        $this->industryImage = $industryImage;

        return $this;
    }

    /**
     * Get industryImage
     *
     * @return string 
     */
    public function getIndustryImage()
    {
        return $this->industryImage;
    }
}
