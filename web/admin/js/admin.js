$(document).ready(function() {
    $('.ui.menu .ui.dropdown').dropdown({
        on: 'hover'
    });
    $('#lhsAnchor')
        .on('click', function() {
            $(this)
                .addClass('active')
                .siblings()
                .removeClass('active');
        });
    $('#createAdmin').click(function(event) {
        event.preventDefault();
        $('#createAdminModal').modal('show');
    });

    // Add admin
    $('#createAdminForm').submit(function(event) {
        $('.newadmin-status').text('');
        $.post(
            $(this).attr('action'),
            $(this).serialize(),     
            function(data) {
                console.log(data);
                var status = jQuery.parseJSON(data);
                if (status.success) {
                    $('.newadmin-status').text('New admin added')
                } 
                else {
                    $('.newadmin-status').text('Admin not added. Please contact with webmaxer74@gmail.com')
            }
        }).fail(function() {
            $('.newadmin-status').text('Admin not added. Please contact with webmaxer74@gmail.com')
          });
        return false;
    });
    // End add admin

    // Update user
    $('body').on('submit', '.js-api_users_update', function (e) {
        e.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize()
        })
        .done(function (data) {
            if (data.image) {
                $('#update_form_image').attr('src', '/'+data.image);
            }
            $('#update_form_body').html(data.html);
            $('#update_form_info').html(data.message);
            vm.$broadcast('vuetable:reload');
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            $('#update_form_info').html(errorThrown);
        });
    });
    // End Update user



});

$('#settingsBtn').on('click', function() {
    $('#menuDiv').removeClass("ui bottom attached segment pushable");
    $('#menuDiv').addClass("ui bottom attached segment");
});

$('#lhsAnchor').on('click', function() {
    $('#menuDiv').removeClass("ui bottom attached segment");
    $('#menuDiv').addClass("ui bottom attached segment pushable");
});

// using context
$('.ui.sidebar')
    .sidebar({
        context: $('.bottom.segment')
    })
    .sidebar('attach events', '.menu .item');

$('#settingsModal').modal({
    detachable: false,
    onVisible: function() {
        $('.ui.checkbox').checkbox()
    }
}).modal('attach events', '#settingsBtn', 'show')



var tableColumns = [{
    name: 'id',
    title: '',
    dataClass: 'center aligned',
    callback: 'showDetailRow'
}, {
    name: 'fullname',
    title: 'Full Name',
    sortField: 'fullname'
}, {
    name: 'location',
    title: 'Location',
    sortField: 'location',
    visible: true
}, {
    name: 'email',
    sortField: 'email'
}, {
    name: 'industry',
    title: 'Industry',
    sortField: 'industry'
}, {
    name: 'phone',
    title: 'Phone',
    sortField: 'phone',
    dataClass: 'nowrap',
}, 
{
    name: 'id',
    title: 'Profile',
    callback: 'showProfile',
}, 
{
    name: 'status',
    title: 'Status',
    sortField: 'status',
    callback: 'showStatus',
    dataClass: 'nowrap',
}, 
{
    name: 'createdAt',
    title: 'Created At',
    sortField: 'createdAt',
    dataClass: 'nowrap',
}, 
// {
//     name: 'created_at',
//     title: 'Last Login',
//     sortField: 'created_at'
// }, 
// {
//     name: 'updated_at',
//     title: 'Last Interview'
// }, 
{
    name: '__component:custom-action',
    title: "Action",
    titleClass: 'center aligned',
    dataClass: 'custom-action center aligned',
}]

Vue.config.debug = true
var E_SERVER_ERROR = 'Error communicating with the server'


Vue.component('custom-action', {
    template: [
        '<div>',
        '<button class="ui icon green button" @click="itemAction(\'edit-item\', rowData)"><i class="edit icon"></i></button>',
        '<button class="ui icon red button" @click="itemAction(\'delete-item\', rowData)"><i class="delete icon"></i></button>',
        '</div>'
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        itemAction: function(action, data) {
            //sweetAlert('Name:', data.fullname)
            // if (action === 'view-item') {
            //     $('.view.modal').modal('show', data);
            //     $('.view.modal').find('#lblName').html(data.fullname);
            //     $('.view.modal').find('#lblIndustry').html(data.industry);
            //     $('.view.modal').find('#lblEmail').html(data.email);
            //     $('.view.modal').find('#lblLocation').html(data.location);
            //     $('.view.modal').find('#lblPhone').html(data.phone);
            //     $('.view.modal').find('#lblImage').attr('src', '/'+data.industryImg);
            // } else 
            if (action === 'edit-item') {
                console.log(data);
                $('.edit.modal').modal('show');
                $('#update_form_info').text('');
                $('#update_form_image').attr('src', '/'+data.industryImg);
                $.ajax({
                    url: App.path.api_user_action+'/'+data.id,
                    type: 'POST',
                    data: {data: data}
                })
                .done(function(data) {
                    console.log(data);
                    $('#update_form_body').html(data.html);
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    console.log('------------------ERROR------------------');
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    console.log('------------------END ERROR------------------');
                    $('#update_form_info').html(errorThrown);
                })              
            } else {
                swal({
                        title: "Are you sure to delete the Candidate " + data.fullname + " ?",
                        text: "You will not be able to recover the data!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function() {
                        $.ajax({
                            url: App.path.api_user_action+'/'+data.id,
                            type: 'DELETE',
                        })
                        .done(function(data) {
                            console.log(data);
                            vm.$broadcast('vuetable:reload');
                            swal("Deleted!", "Candidate successfully deleted.", "success");
                        })
                        .fail(function(jqXHR, textStatus, errorThrown) {
                            console.log('------------------ERROR------------------');
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            console.log('------------------END ERROR------------------');
                            swal("Error!", "Candidate not deleted.", "error");
                        })


                    }
                );
            }
        },
        onClick: function(event) {
            console.log('custom-action: on-click', event.target)
        },
        onDoubleClick: function(event) {
            console.log('custom-action: on-dblclick', event.target)
        },

    }
})

Vue.component('my-detail-row', {
    template: [
        '<div class="detail-row ui form" @click="onClick($event)">',
        '<div class="inline field">',
        '<label>Name: </label>',
        '<span>{{rowData.fullname}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Location: </label>',
        '<span>{{rowData.location}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Email: </label>',
        '<span>{{rowData.email}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Industry: </label>',
        '<span>{{rowData.industry}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Phone: </label>',
        '<span>{{rowData.phone}}</span>',
        '</div>',
        '<div class="inline field">',
        '<label>Skills: </label>',
        '<ul v-for="skill in rowData.skills"><li>{{skill.skillName}}</li></ul>',
        '</div>',
        '<div class="inline field">',
        '<label>Certification: </label>',
        '<ul v-for="certificate in rowData.certificates"><li>{{certificate.certificateName}}</li></ul>',
        '</div>',
        '<div class="inline field">',
        '<label>Email confirmed: </label>',
        '<span>{{rowData.confirmed}}</span>',
        '</div>',
        '</div>',
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        onClick: function(event) {
            console.log('my-detail-row: on-click')
        }
    },
})

var vm = new Vue({
    el: '#app',
    data: {
        searchFor: '',
        moreParams: [],
        fields: tableColumns,
        sortOrder: [{
            field: 'fullname',
            direction: 'asc',
        }],
        multiSort: true,
        paginationComponent: 'vuetable-pagination',
        perPage: 10
    },
    watch: {
        'perPage': function(val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function(val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
        }
    },
    methods: {
        /**
         * Callback functions
         */
        allCap: function(value) {
            return value.toUpperCase()
        },
        gender: function(value) {
            return value == 'M' ? '<span class="ui teal label"><i class="male icon"></i>Male</span>' : '<span class="ui pink label"><i class="female icon"></i>Female</span>'
        },
        formatDate: function(value, fmt) {
            if (value == null) return ''
            fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
            return moment(value, 'YYYY-MM-DD').format(fmt)
        },
        showDetailRow: function(value) {
            var icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'down' : 'right'
            return [
                '<a class="show-detail-row">',
                '<i class="chevron circle ' + icon + ' icon"></i>',
                '</a>'
            ].join('')
        },
        showProfile: function(value) {
            return ['<a class="profile-link" href="../profile/'+value+'">View</a>']
        },
        showStatus: function(value) {
            // var $form = $('#statusForm').clone();
            // $form.find('form').show();
            // $form.find("option[name="+value+"]").attr('selected','selected');
            // return $form.html();
            return value ? 'Verified' : 'In progress';
        },
        setFilter: function() {
            this.moreParams = [
                'filter=' + this.searchFor
            ]
            this.$nextTick(function() {
                this.$broadcast('vuetable:refresh')
            })
        },
        resetFilter: function() {
            this.searchFor = ''
            this.setFilter()
        },
        changePaginationComponent: function() {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
        },
        preg_quote: function(str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
        rowClassCB: function(data, index) {
            return (index % 2) === 0 ? 'odd' : 'even'
        }
        // -------------------------------------------------------------------------------------------
        // You can change how sort params string is constructed by overriding getSortParam() like this
        // -------------------------------------------------------------------------------------------
        // getSortParam: function(sortOrder) {
        //     console.log('parent getSortParam:', JSON.stringify(sortOrder))
        //     return sortOrder.map(function(sort) {
        //         return (sort.direction === 'desc' ? '+' : '') + sort.field
        //     }).join(',')
        // }
    },
    events: {
        'vuetable:row-changed': function(data) {
            console.log('row-changed:', data.fullname)
        },
        'vuetable:row-clicked': function(data, event) {
            // if (event.target.classList.contains('profile-link')) {
            //     this.$broadcast('vuetable:toggle-detail', data.id)
            // }
            console.log('row-clicked:', data.fullname)
        },
        'vuetable:cell-clicked': function(data, field, event) {
            if (field.name === 'id') {
                this.$broadcast('vuetable:toggle-detail', data.id)
            }
        },
        'vuetable:action': function(action, data) {
            console.log('vuetable:action', action, data)

            if (action == 'view-item') {
                sweetAlert(action, data.fullname)
            } else if (action == 'edit-item') {
                sweetAlert(action, data.fullname)
            } else if (action == 'delete-item') {
                sweetAlert(action, data.fullname)
            }
        },
        'vuetable:load-success': function(response) {
            var data = response.data.data
            if (this.searchFor !== '') {
                for (n in data) {
                    data[n].fullname = this.highlight(this.searchFor, data[n].fullname);
                    data[n].email = this.highlight(this.searchFor, data[n].email);
                    data[n].industry = this.highlight(this.searchFor, data[n].industry);
                }
            }
        },
        'vuetable:load-error': function(response) {
            if (response.status == 400) {
                sweetAlert('Something\'s Wrong!', response.data.message, 'error')
            } else {
                console.log(response);
                sweetAlert('Oops', E_SERVER_ERROR, 'error')
            }
        }
    }


})

