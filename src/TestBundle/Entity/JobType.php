<?php

namespace TestBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * JobType
 *
 * @ORM\Table(name="job_type")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\JobTypeRepository")
 */
class JobType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     * @ORM\Column(name="jobTypeTitle", type="string", length=255)
     */
    private $jobTypeTitle;


    /**
     * @ORM\ManyToMany(targetEntity="JobPreference", mappedBy="jobTypes", cascade={"persist"}, orphanRemoval=true)
     */
    private $jobPreferences;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jobPreferences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jobTypeTitle
     *
     * @param string $jobTypeTitle
     * @return JobType
     */
    public function setJobTypeTitle($jobTypeTitle)
    {
        $this->jobTypeTitle = $jobTypeTitle;

        return $this;
    }

    /**
     * Get jobTypeTitle
     *
     * @return string 
     */
    public function getJobTypeTitle()
    {
        return $this->jobTypeTitle;
    }

    /**
     * Add jobPreferences
     *
     * @param \TestBundle\Entity\JobPreference $jobPreferences
     * @return User
     */
    public function addJobPreference(\TestBundle\Entity\JobPreference $jobPreference)
    {
        if (! $this->jobPreferences->contains($jobPreference)) {
          $jobPreference->setJobType($this);
          $this->jobPreferences->add($jobPreference);
        }

        return $this->jobPreferences;
    }

    /**
     * Remove jobPreferences
     *
     * @param \TestBundle\Entity\JobPreference $jobPreferences
     */
    public function removeJobPreference(\TestBundle\Entity\JobPreference $jobPreferences)
    {
        $this->jobPreferences->removeElement($jobPreferences);
    }

    /**
     * Get jobPreferences
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobPreferences()
    {
        return $this->jobPreferences;
    }
}
