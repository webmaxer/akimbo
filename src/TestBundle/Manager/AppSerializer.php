<?php

namespace TestBundle\Manager;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class AppSerializer
{
    /**
     * Normalizer
     *
     * @var \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer
     */
    private $normalizer;

    /**
     * JsonEncoder
     *
     * @var \Symfony\Component\Serializer\Encoder\JsonEncoder
     */
    private $encoder;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->normalizer = new GetSetMethodNormalizer();
        $this->encoder    = new JsonEncoder();
    }
    
    public function serializeJobs(array $jobs)
    {
        $this->normalizer->setIgnoredAttributes(['jobs', 'jobPreferences', 'user', 'users']);
        
        $serializer = new Serializer([$this->normalizer], [$this->encoder]);

        return $serializer->serialize($jobs, 'json');
    }
}