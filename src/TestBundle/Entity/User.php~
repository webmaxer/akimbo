<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints as MisdAssert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
/**
 * User
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"email"},
 *     message="This E-Mail address has already been registered",
 *     groups={"registration"} 
 * )
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank( message = "First name should not be blank", groups={"registration", "admin_update"} )
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank( message = "Last name should not be blank", groups={"registration", "admin_update"} )
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;


    /**
     * @Assert\NotBlank( message = "Phone should not be blank"  )
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @Assert\NotBlank( message = "Email should not be blank", groups={"registration"} )
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true,
     *     groups={"registration", "admin_update"}
     * )
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank( message = "Password should not be blank", groups={"registration"} )
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(name="passwordReset", type="string", length=255)
     */
    private $passwordReset;

    /**
     * @var string
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var string
     * @Assert\NotBlank( message = "Your current location should not be blank"  )
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @var string
     * @Assert\NotBlank( message = "Description should not be blank" )
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="Company", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $companies;

    /**
     * @ORM\OneToMany(targetEntity="Education", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $educations;

    /**
     * @ORM\OneToMany(targetEntity="Reference", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $references;


    /**
     * @ORM\ManyToOne(targetEntity="Template", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;


    /**
     * @ORM\OneToOne(targetEntity="JobPreference", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $jobPreference;


    /**
     * @ORM\ManyToOne(targetEntity="Industry", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(name="industry_id", referencedColumnName="id")
     */
    private $industry;

    /**
     * @ORM\ManyToMany(targetEntity="Skill", inversedBy="users", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinTable(name="users_skills",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id")}
     *      )
     *
     * @Assert\Count(
     *      min = "1",
     *      max = "5",
     *      minMessage = "You must specify at least one skill",
     *      maxMessage = "You cannot specify more than {{ limit }} skills"
     * )
     */
    private $skills;


    /**
     * @ORM\ManyToMany(targetEntity="Certificate", inversedBy="users")
     * @ORM\JoinTable(name="users_certificates",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="certificate_id", referencedColumnName="id")}
     *      )
     */
    private $certificates;

    /**
     * @var boolean
     * @Assert\IsTrue(message = "Your email is not confirmed")
     * @ORM\Column(name="confirmed", type="boolean")
     */
    private $isConfirmed = false;

    /**
     * @var boolean
     * @Assert\IsTrue(message = "Your profile is not verified")
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = false;

    /**
     * @ORM\OneToOne(targetEntity="ConfirmList", mappedBy="userId")
     */
    protected $confirm;



    public function __construct()
    {
        $this->companies = new ArrayCollection();
        $this->educations = new ArrayCollection();
        $this->skills = new ArrayCollection();
        $this->certificates = new ArrayCollection();
        $this->references = new ArrayCollection();
        $this->salt = md5(time());
        $this->setCreatedAt(new \DateTime());
    }


    public function eraseCredentials()
    {
    
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->email;
    }


    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /** @see \Serializable::serialize() */
     public function serialize()
     {
         return serialize(array(
             $this->id,
             $this->email,
             $this->password,

         ));
     }


     /** @see \Serializable::unserialize() */
     public function unserialize($serialized)
     {
         list (
             $this->id,
             $this->email,
             $this->password,
             // see section on salt below
             // $this->salt
         ) = unserialize($serialized);
     }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add companies
     *
     * @param \TestBundle\Entity\Company $companies
     * @return User
     */
    public function addCompany(\TestBundle\Entity\Company $company)
    {
        if (! $this->companies->contains($company)) {
          $company->setUser($this);
          $this->companies->add($company);
        }

        return $this->companies;
    }

    /**
     * Remove companies
     *
     * @param \TestBundle\Entity\Company $companies
     */
    public function removeCompany(\TestBundle\Entity\Company $companies)
    {
        $this->companies->removeElement($companies);
    }

    /**
     * Get companies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add educations
     *
     * @param \TestBundle\Entity\Education $educations
     * @return User
     */
    public function addEducation(\TestBundle\Entity\Education $education)
    {
        if (! $this->educations->contains($education)) {
          $education->setUser($this);
          $this->educations->add($education);
        }

        return $this->educations;
    }

    /**
     * Remove educations
     *
     * @param \TestBundle\Entity\Education $educations
     */
    public function removeEducation(\TestBundle\Entity\Education $educations)
    {
        $this->educations->removeElement($educations);
    }

    /**
     * Get educations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEducations()
    {
        return $this->educations;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {   

        // $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        // $passCrypted = $encoder->encodePassword($password, $this->salt);
        // $this->password = $passCrypted;
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Add skills
     *
     * @param \TestBundle\Entity\Skill $skills
     * @return User
     */
    public function addSkill(\TestBundle\Entity\Skill $skill)
    {
        if (! $this->skills->contains($skill)) {
          $skill->setUser($this);
          $this->skills->add($skill);
        }

        return $this->skills;
    }

    /**
     * Remove skills
     *
     * @param \TestBundle\Entity\Skill $skills
     */
    public function removeSkill(\TestBundle\Entity\Skill $skills)
    {
        $this->skills->removeElement($skills);
    }

    /**
     * Get skills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Add certificates
     *
     * @param \TestBundle\Entity\Certificate $certificates
     * @return User
     */
    public function addCertificate(\TestBundle\Entity\Certificate $certificates)
    {
        $this->certificates[] = $certificates;

        return $this;
    }

    /**
     * Remove certificates
     *
     * @param \TestBundle\Entity\Certificate $certificates
     */
    public function removeCertificate(\TestBundle\Entity\Certificate $certificates)
    {
        $this->certificates->removeElement($certificates);
    }

    /**
     * Get certificates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCertificates()
    {
        return $this->certificates;
    }

    /**
     * Set industry
     *
     * @param \TestBundle\Entity\Industry $industry
     * @return User
     */
    public function setIndustry(\TestBundle\Entity\Industry $industry = null)
    {
        $this->industry = $industry;

        return $this;
    }

    /**
     * Get industry
     *
     * @return \TestBundle\Entity\Industry 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return User
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add references
     *
     * @param \TestBundle\Entity\Reference $references
     * @return User
     */
    public function addReference(\TestBundle\Entity\Reference $reference)
    {
        if (! $this->references->contains($reference)) {
          $reference->setUser($this);
          $this->references->add($reference);
        }

        return $this->references;
    }

    /**
     * Remove references
     *
     * @param \TestBundle\Entity\Reference $references
     */
    public function removeReference(\TestBundle\Entity\Reference $references)
    {
        $this->references->removeElement($references);
    }

    /**
     * Get references
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReferences()
    {
        return $this->references;
    }



    /**
     * Set jobPreference
     *
     * @param \TestBundle\Entity\JobPreference $jobPreference
     * @return User
     */
    public function setJobPreference(\TestBundle\Entity\JobPreference $jobPreference = null)
    {
        $this->jobPreference = $jobPreference;
        $jobPreference->setUser($this);
        return $this;
    }

    /**
     * Get jobPreference
     *
     * @return \TestBundle\Entity\JobPreference 
     */
    public function getJobPreference()
    {
        return $this->jobPreference;
    }

    /**
     * Set template
     *
     * @param \TestBundle\Entity\Template $template
     * @return User
     */
    public function setTemplate(\TestBundle\Entity\Template $template = null)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * Get template
     *
     * @return \TestBundle\Entity\Template 
     */
    public function getTemplate()
    {
        return $this->template;
    }





    /**
     * Set isConfirmed
     *
     * @param boolean $isConfirmed
     * @return User
     */
    public function setIsConfirmed($isConfirmed)
    {
        $this->isConfirmed = $isConfirmed;

        return $this;
    }

    /**
     * Get isConfirmed
     *
     * @return boolean 
     */
    public function getIsConfirmed()
    {
        return $this->isConfirmed;
    }

    /**
     * Set confirm
     *
     * @param \TestBundle\Entity\ConfirmList $confirm
     * @return User
     */
    public function setConfirm(\TestBundle\Entity\ConfirmList $confirm = null)
    {
        $this->confirm = $confirm;

        return $this;
    }

    /**
     * Get confirm
     *
     * @return \TestBundle\Entity\ConfirmList 
     */
    public function getConfirm()
    {
        return $this->confirm;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {   
        if ($this->createdAt!=null) {
            $date = $this->createdAt->format('m/d/Y H:i');
        }
        else {
            $date = null;
        }
        return $date;
    }



    /**
     * Set status
     *
     * @param boolean $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
