var JobsCollection = Backbone.Collection.extend({
    model: JobModel, 
    url: function() {
        return '/app.php/api/jobs';
    },
   	page: {
   		current: 1,
   		count: 1
   	},
   	parse: function(response) {
   		var jobs = $.parseJSON(response.jobs);

   		//this.page.count   = response.pages;
   		//this.page.current = response.page;

   		for(key in jobs) {
   			this.push(jobs[key]);
   		}

   		return this.models;
   	},
   	// sync:   function(path, method, data) {
	// 	    console.log(path);
	// 	    console.log(method);
	// 	    console.log(data);
	// },
});