<?php 


namespace TestBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobPreferenceType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('idealJobTitle');
        $builder->add('location', 'text', array(
            'error_bubbling' => true,
            'attr'=> array(
                'class'=>'autocomplete-input js-location'
            ),
        ));
        $builder->add('salary', 'integer', [
            'label'=>'Salary Required'
        ]);
        $builder->add('industries', 'entity', array(
            'class' => 'TestBundle:Industry',
            'property' => 'industryName',
            'expanded' => false,
            'multiple' => true,
            'required' => false,
            'error_bubbling' => true,
        ));
        $builder->add('jobTypes', 'entity', array(
            'label' => 'Job type',
            'class' => 'TestBundle:JobType',
            'property' => 'jobTypeTitle',
            'expanded' => false,
            'multiple' => true,
            'required' => false,
            'error_bubbling' => true,
        ));


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\JobPreference',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'jobPreference';
    }
}