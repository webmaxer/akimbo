<?php

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use TestBundle\Form\Type\changePasswordType;
use TestBundle\Form\Type\RegistrationType;
use TestBundle\Form\Type\ResetPasswordType;
use TestBundle\Entity\User;
use TestBundle\Entity\ConfirmList;


class BuilderController extends Controller
{

 
    private function sendMail($user, $key) 
    {
      $email = $user->getEmail();

      $message = \Swift_Message::newInstance()
           ->setSubject('Hello Email')
           ->setFrom('mail@akimboworks.com')
           ->setTo($email);

      $images['logo'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/akimbo_logo.png'));
      $images['f'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/f.png'));
      $images['t'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/t.png'));
      $images['m'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/m.png'));
      $images['in'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/in.png'));
      $message->setBody(
               $this->renderView(
                   'TestBundle:Emails:registration.html.twig',
                   array('firstname' => $user->getFirstName(), 'lastname' => $user->getLastName(), 'images'=>$images, 'key'=>$key)
               ),
               'text/html'
           );

        $message->setEncoder(\Swift_Encoding::getBase64Encoding());
      return $this->get('mailer')->send($message);
    }

    /**
     * @Route("/builder", name="builder")
     */
    public function builderAction(Request $request)
    {   

        $user = new User();

        $form = $this->createForm(new RegistrationType($this->getDoctrine()->getManager()), $user);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();

                $em->persist($user);

                $key = $user->getFirstName() . $user->getEmail() . date('mY');
                $key = md5($key);

                $confirm = new ConfirmList();
                $confirm->setUserId($user);
                $confirm->setActivationKey($key);
                $em->persist($confirm);

                $em->flush();

                $this->sendMail($user, $key);

                $id = $user->getId();
                $providerKey = 'user'; // your firewall name
                $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());
                $this->container->get('security.context')->setToken($token);

                return $this->redirect($this->generateUrl('home'));
            }
            else {
                return $this->render('TestBundle:Default:builder.html.twig', array(
                  'form' => $form->createView(),
                ));
            }
        } 
        else {
            return $this->render('TestBundle:Default:builder.html.twig', array(
              'form' => $form->createView(),
            ));
        }


    }



    /**
     * @Route("/confirm/{key}", name="confirm")
     */
    public function confirmAction(Request $request, $key) 
    {
      $em = $this->getDoctrine()->getManager();
      $confirm = $em->getRepository('TestBundle:ConfirmList')->findOneByActivationKey($key);
      if ($confirm) {
        $id = $confirm->getUserId()->getId();
        $user = $em->getRepository('TestBundle:User')->find($id);
        $user->setIsConfirmed(true);

        $em->remove($confirm);
        $em->persist($user);
        $em->flush();

        return $this->redirect($this->generateUrl('profile', array('id' => $id)));
      }
      else {
        return $this->redirect($this->generateUrl('login'));
      }

    }

    /**
     * @Route("/sendconfirm/{key}", name="sendconfirm")
     */
    public function sendConfirmAction(Request $request, $key) 
    {
      $em = $this->getDoctrine()->getManager();
      $confirm = $em->getRepository('TestBundle:ConfirmList')->findOneByActivationKey($key);
      if ($confirm) {
        $id = $confirm->getUserId()->getId();
        $user = $em->getRepository('TestBundle:User')->find($id);
        $this->sendMail($user, $key);
        return $this->redirect($this->generateUrl('profile', array('id' => $id)));
      }

    }
    /**
     * @Route("/changePassword/{id}", name="password")
     */
    public function changePasswordAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('TestBundle:User')->find($id);
        $user->setPassword(null);
        $form = $this->createForm(new changePasswordType($em), $user);


        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {


                $em->persist($user);
                $em->flush($user);
                return $this->redirect($this->generateUrl('login'));

            }


        }


        return $this->render('TestBundle:Default:changePassword.html.twig', array(
            'form' => $form->createView(),
        ));


    }


    /**
     * @Route("/reset-password/", name="reset-password")
     */
    public function resetPasswordAction(Request $request)
    {   
      $em = $this->getDoctrine()->getManager();
      $user = new User();
      $form = $this->createForm(new ResetPasswordType($em), $user);
      if ( $request->isMethod('POST') ) {

        $form->handleRequest($request);

        $email = $form->getData()->getEmail();

        $user = $em->getRepository('TestBundle:User')->findOneByEmail($email);
           
          $em->persist($user);
          $em->flush($user);
        if ($form->isValid() && $user!=null) {

          $message = \Swift_Message::newInstance()
               ->setSubject('Hello Email')
               ->setFrom('mail@akimboworks.com')
               ->setTo($email);

          $images['logo'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/akimbo_logo.png'));
          $images['f'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/f.png'));
          $images['t'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/t.png'));
          $images['m'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/m.png'));
          $images['in'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/in.png'));
          $message->setBody(
            $this->renderView
            (
              'TestBundle:Emails:reset_password.html.twig',
                [
                'firstname' => $user->getFirstName(), 
                'lastname' => $user->getLastName(), 
                'passwordReset' => $user->getPasswordReset(), 
                'images'=>$images,
                'id' => $user->getId()
                ]
            ), 
            'text/html'
          );
            $message->setEncoder(\Swift_Encoding::getBase64Encoding());

          $this->get('mailer')->send($message);
            $session = new Session();
            $session->getFlashBag()->add('ResetSent', 'An email has been sent to this email address to reset your password  ');

            return $this->render('TestBundle:Default:reset-password.html.twig', ['form'=>$form->createView(), 'sent'=>true]);
        }
        else {     

          return $this->render('TestBundle:Default:reset-password.html.twig', ['form'=>$form->createView(), 'error'=>true, 'email'=>$email]);
        }
      }
      else {
        return $this->render('TestBundle:Default:reset-password.html.twig', ['form'=>$form->createView()]);
      }
    }


}
