<?php

namespace TestBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use TestBundle\Form\Type\UserType;
use TestBundle\Entity\User;
use TestBundle\Entity\Company;
use TestBundle\Entity\Education;
use TestBundle\Entity\Skill;
use TestBundle\Entity\Industry;
use TestBundle\Entity\Reference;

class UtilsController extends Controller
{

    /**
     * @Route("/re-configure-bages", name="re-configure-bages")
     */
    public function badgeAction(Request $request) {
        $dir = 'images/industry/';

        function clean($string) {
            $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
            $string = str_replace(' ', '-', $string);
            $string = str_replace('--', '-', $string);
            return $string;
        }

        $em = $this->getDoctrine()->getManager();
        $industries = $em->getRepository('TestBundle:Industry')->findAll();
        $images = [];
        if ($handle = opendir($dir)) {

            while (false !== ($entry = readdir($handle))) {

                if ($entry != "." && $entry != "..") {

                    $images[] = $entry;
                }
            }

            closedir($handle);
        }

        foreach ($industries as $key => $value) {
            $industry = $value->getIndustryName();
            $industry = clean($industry);
 
            foreach ($images as $key => $image) {
                if (strpos($image, $industry) !== false) {
                    $value->setIndustryImage($dir.$image);
                    $em->persist($value);
                }
            }
        }
        $em->flush();

        return new Response();
    }


    /**
     * @Route("/email-test", name="email")
     */
    public function emailTestAction()
    {   

        $message = \Swift_Message::newInstance()
                ->setSubject('Hello Email')
                ->setFrom('danielle@akimboconnect.com')
                ->setTo('webmaxer74@gmail.com');

        $images['logo'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/akimbo_logo.png'));
        $images['f'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/f.png'));
        $images['t'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/t.png'));
        $images['m'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/m.png'));
        $images['in'] = $message->embed(\Swift_Image::fromPath(__DIR__ . '/../Resources/views/Emails/in.png'));
        $message->setBody(
                    $this->renderView(
                        'TestBundle:Emails:registration.html.twig',
                        array('firstname' => 'Max', 'lastname'=>'Chuhnov', 'password' => 'paaw', 'images'=>$images)
                    ),
                    'text/html'
                );
        $this->get('mailer')->send($message);

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('TestBundle:User')->findAll();

        return $this->render('TestBundle:Default:admin.html.twig', array(
            'users'=>$users
        ));

    }

}
