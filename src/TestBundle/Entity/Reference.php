<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Misd\PhoneNumberBundle\Validator\Constraints as MisdAssert;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Reference
 *
 * @ORM\Table(name="reference")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\ReferenceRepository")
 */
class Reference
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank( message = "Reference full name should not be blank" )
     * @ORM\Column(name="full_name", type="string", length=255)
     */
    private $fullName;

    /**
     * @var string
     * @Assert\NotBlank( message = "Reference job title should not be blank" )
     * @ORM\Column(name="job_title", type="string")
     */
    private $jobTitle;

    /**
     * @var string
     * @Assert\NotBlank( message = "Reference organization should not be blank" )
     * @ORM\Column(name="organization", type="string")
     */
    private $organization;


    /**
     * @var string
     * @Assert\NotBlank( message = "Reference relationship should not be blank" )
     * @ORM\Column(name="relationship", type="string")
     */
    private $relationship;

    /**
     * @Assert\NotBlank( message = "Reference phone should not be blank" )
     * @ORM\Column(type="string", length=50)
     * @MisdAssert\PhoneNumber()
     */
    private $phone;


    /**
     * @var string
     * @Assert\NotBlank( message = "Reference email should not be blank" )
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="references")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set yearFinished
     *
     * @param \Date $yearFinished
     * @return Education
     */
    public function setYearFinished($yearFinished)
    {
        $this->yearFinished = $yearFinished;

        return $this;
    }

    /**
     * Get yearFinished
     *
     * @return \Date
     */
    public function getYearFinished()
    {
        return $this->yearFinished;
    }

    /**
     * Set user
     *
     * @param \TestBundle\Entity\User $user
     * @return Education
     */
    public function setUser(\TestBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TestBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set instituteName
     *
     * @param string $instituteName
     * @return Education
     */
    public function setInstituteName($instituteName)
    {
        $this->instituteName = $instituteName;

        return $this;
    }

    /**
     * Get instituteName
     *
     * @return string 
     */
    public function getInstituteName()
    {
        return $this->instituteName;
    }

    /**
     * Set degree
     *
     * @param string $degree
     * @return Education
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * Get degree
     *
     * @return string 
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Education
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return Reference
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     * @return Reference
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string 
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set organization
     *
     * @param string $organization
     * @return Reference
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return string 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Reference
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Reference
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set relationship
     *
     * @param string $relationship
     * @return Reference
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;

        return $this;
    }

    /**
     * Get relationship
     *
     * @return string 
     */
    public function getRelationship()
    {
        return $this->relationship;
    }
}
