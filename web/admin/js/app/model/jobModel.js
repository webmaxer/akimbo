var JobModel = Backbone.Model.extend({
    defaults: {
        id: 0,
    },
    getCustomUrl: function (method) {
        switch (method) {
            case 'read':
                return '/app.php/api/jobs/' + this.id;
                break;
            case 'update':
                return '/app.php/api/jobs/' + this.id;
                break;
            case 'delete':
                return '/app.php/api/jobs/' + this.id;
                break;
        }
    },
    save: function(collection) {
        $.ajax({
            url: window.location+'new',
            type: 'POST',
            data: { job: this.toJSON() },
            dataType: 'JSON',
            success: function(data) {
               $('#wrapper_popup').removeClass('visible');
                $('#wrapper_popup').removeClass('active');
                $('#add_form_job').removeClass('visible');
                $('#add_form_job').removeClass('active');
                collection.reset();
                jobsListView.clearList();
                collection.fetch();
            },
            error: function(xhr, status) {
                //
            }
        });
    },
    update: function() {
        $.ajax({
            url:  '/app.php/api/jobs/' + this.id,
            type: 'POST',
            data: { job: this.toJSON() },
            dataType: 'JSON',
            success: function(data) {
               $('#wrapper_popup').removeClass('visible');
                $('#wrapper_popup').removeClass('active');
                $('#edit_form_job').removeClass('visible');
                $('#edit_form_job').removeClass('active');

                jobsCollection.reset();
                jobsListView.clearList();
                jobsCollection.fetch();
            },
            error: function(xhr, status) {
                //
            }
        });
    },
    sync: function (method, model, options) {
        options || (options = {});
        options.url = this.getCustomUrl(method.toLowerCase());
        
        return Backbone.sync.apply(this, arguments);
    }
});