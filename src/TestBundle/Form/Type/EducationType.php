<?php 


namespace TestBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class EducationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('instituteName');
        $builder->add('dateFinished', DateType::class, array(
            'widget' => 'single_text',
            'format' => 'dd-MM-yyyy',
            
            'attr' => array(
              'class' => 'datepicker',
            ),
            'required' => true,
        ));
        $builder->add('location', 'text', array(
            'attr'=> array(
                'class'=>'autocomplete-input js-location'
            )
        ));
        $builder->add('degree');

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Education',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'education';
    }
}