<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Job
 *
 * @ORM\Table(name="job")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\JobRepository")
 */
class Job
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="jobTitle", type="string")
     */
    private $jobTitle = null;

    /**
     * @var string
     * @ORM\Column(name="salary", type="integer", nullable=true)
     */
    private $salary = null;

    /**
     * @var string
     * @ORM\Column(name="location", type="string", nullable=true)
     */
    private $location = null;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description = null;

    /**
     * @var string
     * @ORM\Column(name="created", type="datetime")
     */
    private $created = null;

    /**
     * @ORM\ManyToMany(targetEntity="JobType", inversedBy="jobs")
     * @ORM\JoinTable(name="jobs_jobTypes",
     *      joinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="jobType_id", referencedColumnName="id")}
     *      )
     */
    private $jobTypes;

    /**
     * @ORM\ManyToMany(targetEntity="Industry", inversedBy="jobs")
     * @ORM\JoinTable(name="jobs_Industries",
     *      joinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="industry_id", referencedColumnName="id")}
     *      )
     */
    private $industries;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="jobs", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jobTypes   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->industries = new \Doctrine\Common\Collections\ArrayCollection();
    }

     /**
     * Set jobTitle
     *
     * @param string $jobTitle
     * @return Job
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string 
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Job
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set salary
     *
     * @param integer $salary
     * @return JobPreference
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return integer 
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set description
     *
     * @param integer $description
     * @return Job
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return Job
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \Datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }   

    /**
     * Set company
     *
     * @param \TestBundle\Entity\Company $company
     * @return Job
     */
    public function setCompany(\TestBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \TestBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add jobTypes
     *
     * @param \TestBundle\Entity\JobType $jobTypes
     * @return Job
     */
    public function addJobType(\TestBundle\Entity\JobType $jobTypes)
    {
        $this->jobTypes[] = $jobTypes;

        return $this;
    }

    /**
     * Remove jobTypes
     *
     * @param \TestBundle\Entity\JobType $jobTypes
     */
    public function removeJobType(\TestBundle\Entity\JobType $jobTypes)
    {
        $this->jobTypes->removeElement($jobTypes);
    }

    /**
     * Get jobTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobTypes()
    {
        return $this->jobTypes;
    }

    /**
     * Add industries
     *
     * @param \TestBundle\Entity\Industry $industries
     * @return Job
     */
    public function addIndustry(\TestBundle\Entity\Industry $industries)
    {
        $this->industries[] = $industries;

        return $this;
    }

    /**
     * Remove industries
     *
     * @param \TestBundle\Entity\Industry $industries
     */
    public function removeIndustry(\TestBundle\Entity\Industry $industries)
    {
        $this->industries->removeElement($industries);
    }

    /**
     * Get industries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIndustries()
    {
        return $this->industries;
    }
}