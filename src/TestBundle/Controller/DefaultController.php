<?php

namespace TestBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use TestBundle\Utils\DocxGenerator\DocxGenerator;
use TestBundle\Form\Type\UserType;
use TestBundle\Entity\User;
use TestBundle\Entity\Company;
use TestBundle\Entity\Education;
use TestBundle\Entity\Skill;
use TestBundle\Entity\Industry;
use TestBundle\Entity\Reference;
use TestBundle\Entity\JobPreference;



class DefaultController extends Controller
{

    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request) {
        $security = $this->get('security.context');
        if ($security->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('dashboard'));
        }
        if ($security->isGranted('ROLE_USER')) {
            $id = $this->get('security.token_storage')->getToken()->getUser()->getId();
            return $this->redirect($this->generateUrl('profile', ['id'=>$id]));
        }
        return $this->render('TestBundle:Default:index.html.twig');
    }



    /**
     * @Route("/profile/{id}/", name="profile")
     * @Route("/admin/profile/{id}/", name="profile_admin")
     */
    public function profileAction(Request $request, $id=null)
    {   

        if ($id==null) {
            return $this->redirect($this->generateUrl('home'));
        }

        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $user = $em->getRepository('TestBundle:User')->find($id);
        } 
        else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        }


        $template = $em->getRepository('TestBundle:Template')->find(1);

        if ($user->getId()) {

            if(!empty($user->getDocument()))
                $user->setDocument(
                    new File($this->getParameter('brochures_directory').'/'.$user->getDocument())
                );

            $form = $this->createForm(new UserType($em), $user);
            $tmp = $user->getDocument();
            $form->handleRequest($request);
            if($user->getDocument()== null){
                $user->setDocument($tmp);
                unset($tmp);
            }


            if ($form->isValid()) {

                $user->setTemplate($template);

                    if ($user->getDocument())  {


                        // $file stores the uploaded PDF file
                        /** @var UploadedFile $file */
                        $file = $user->getDocument();

                        // Generate a unique name for the file before saving it
                        $fileName = 'User'.$user->getId().'doc' . '.' . $file->guessExtension();

                        // Move the file to the directory where document are stored
                        $file->move(
                            $this->getParameter('brochures_directory'),
                            $fileName
                        );

                        // Update the 'document' property to store the PDF file name
                        // instead of its contents
                        $user->setDocument($fileName);
                    }

                $em->persist($user);
                $em->flush();

                if ($form->get('goToPreview')->isClicked()) {
                    if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
                        return $this->redirect($this->generateUrl('profile-preview_admin', ['id'=>$id]));
                    } 
                    else {
                        return $this->redirect($this->generateUrl('profile-preview', ['id'=>$id]));
                    }
                }
                else {


                    return $this->render('TestBundle:Default:profile.html.twig', array(
                        'form' => $form->createView(),
                        'user' => $user,
                        'isProfileUpdated' => true
                    ));
                }
            }
            else {

                return $this->render('TestBundle:Default:profile.html.twig', array(
                  'form' => $form->createView(),
                  'user' => $user,
                ));
            }
        }
        else {
            return $this->redirect($this->generateUrl('home'));
        }
    }







    /**
     * @Route("/profile/{id}/preview/", name="profile-preview")
     * @Route("/admin/profile/{id}/preview/", name="profile-preview_admin")
     */
    public function previewResumeAction (Request $request, $id=null)
    {   

        $em = $this->getDoctrine()->getManager();
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $user = $em->getRepository('TestBundle:User')->find($id);
        } 
        else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        }

        $templates = $em->getRepository('TestBundle:Template')->findAll();

        if ($user) {
            return $this->render('TestBundle:Default:preview-resume.html.twig', array(
                'user' => $user,
                'templates' => $templates
            ));
        }
        else {
            return $this->redirect($this->generateUrl('home'));
        }

    }


    /**
     * @Route("/profile-downloadPdf/{id}/", name="download-pdf")
     * @Route("/admin/profile-downloadPdf/{id}/", name="download-pdf_admin")
     */
    public function downloadPdfAction (Request $request, $id=null)
    {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('TestBundle:User')->find($id);
        } 
        else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        }
        $templateName = $user->getTemplate()->getName();

        $snappy = $this->get('knp_snappy.pdf');
        $options = [
            'margin-top'    => 0,
            'margin-right'  => 0,
            'margin-bottom' => 0,
            'margin-left'   => 0,
        ];
        $html = $this->renderView(
            'TestBundle:ResumeTemplates:PDF/template.html.twig',
            array(
                'user'  => $user,
                'template' => $templateName
            )
        );
        
        return new Response(
            $snappy->getOutputFromHtml($html, $options),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="Resume for '.$user->getFirstName().' '.$user->getLastName(). '.pdf'
            )
        );
    }

    /**
     * @Route("/profile-downloadDocx/{id}/", name="download-docx")
     * @Route("/admin/profile-downloadDocx/{id}/", name="download-docx_admin")
     */
    public function downloadDocxAction (Request $request, $id=null) 
    {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('TestBundle:User')->find($id);
        } 
        else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        }

        $templateName = $user->getTemplate()->getName();
        $html = $this->renderView("TestBundle:ResumeTemplates:DOCX/$templateName.html.twig", ['user'  => $user]);
        $options = [
            // 'marginTop'    => 0,
            // 'marginRight'  => 0,
            // 'marginBottom' => 0,
            // 'marginLeft'   => 0,
        ];
        $styles = $this->get('kernel')->getRootDir()."/../src/TestBundle/Resources/views/ResumeTemplates/DOCX/styles/$templateName.php"; 
        $generator = new DocxGenerator($html, $options, $styles);
        $path = 'DOCX/user/'.$user->getId().'/';
        if (!file_exists($path)) {
            if (!mkdir($path, 0777, true)) {
                die('Cant create folder');
            }
        }
        $filename = 'Resume for '.$user->getFirstName().' '.$user->getLastName().'.docx';
        $file = $path.$filename;
        $generator->generateDocx($file);

        return new Response(
            file_get_contents($file),
            200,
            array(
                'Content-Type'          => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'Content-Disposition'   => 'attachment; filename="Resume for '.$user->getFirstName().' '.$user->getLastName(). '.docx'
            )
        );

    }


    /**
     * @Route("/profile/{id}/template{templateId}/", name="set-template-action")
     * @Route("/admin/profile/{id}/template{templateId}/", name="set-template-action_admin")
     */
    public function setTemplateAction (Request $request, $id, $templateId) 
    {

        try {
            $em = $this->getDoctrine()->getManager();
            if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
                $user = $em->getRepository('TestBundle:User')->find($id);
            } 
            else {
                $user = $this->get('security.token_storage')->getToken()->getUser();
            }
            $template = $em->getRepository('TestBundle:Template')->find($templateId);
            $user->setTemplate($template);
            $em->persist($user);
            $em->flush();
            $response = array("code" => 100, "success" => true);
        } catch (Exception $e) {
            $response = array("code" => 500, "success" => false, "message"=>$e);
        }


        return new Response(json_encode($response));
        

    }


    /**
     * @Route("/hirePage/", name="hirePage")
     */
    public function hirePageAction ()
    {  
        return $this->render('TestBundle:Default:hire.html.twig');
    }


    /**
     * @Route("/jobs/", name="jobs")
     */
    public function jobsAction ()
    {  
        return $this->render('TestBundle:Default:jobs.html.twig');
    }




}
